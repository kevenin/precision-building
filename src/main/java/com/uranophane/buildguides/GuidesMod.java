package com.uranophane.buildguides;

import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.RenderTickEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.RenderGlobal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.uranophane.buildguides.cmd.CmdHelp;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.TransformGui;
import com.uranophane.buildguides.gui.modes.Bezier;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.gui.modes.ModeProxy;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideRays;
import com.uranophane.buildguides.proxy.ClientProxy;
import com.uranophane.buildguides.proxy.CommonProxy;
import com.uranophane.buildguides.util.RenderTools;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;



@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION)
public class GuidesMod {
	
	private static final String SAVEPATH = "guides\\";
	private static final int MAXTRACE = 150;
	private static final float RENDER_THRESH = 0.14F;
	public static float red = 0F;
	public static float green = 1F;
	public static float blue = 0F;
	public static float alpha = 0.9F;
	
	public static float drawDist = 10F;
	
	protected static double relX;
	protected static double relY;
	protected static double relZ;
	
	public static BlockPos currentBlock = new BlockPos(0, 0, 0);
	public static float partialTicks;
	public static EntityPlayer player;
	public static String worldId;
	private static boolean renderSync = true;
	
	public static ArrayList<Guide> guideList = new ArrayList();
	public static SettingsGui settingsGui = new SettingsGui();
	
	public static HashMap<String, KeyBinding> keyBinds = ClientProxy.keyBinds;
	
	public static void registerGuide(Guide g) {
		guideList.add(g);
	}
	
	public static void updateVBOs() {
		for (Guide guide : guideList) {
			guide.updateCheapVBO();
		}
	}
	
	public static void updateDynamicVBOs() {
		for (Guide guide : guideList) {
			if (!guide.isInitialized())
				guide.initializeInstance();
			guide.updateDynamicVBO();
		}
	}
	
	@SidedProxy(clientSide = Reference.CLIENTPROXY, serverSide = Reference.COMMONPROXY)
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
		proxy.registerTickHandler();
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	//rendering event
	@SubscribeEvent
	public void onDrawBlockSelectionBox(DrawBlockHighlightEvent e) {
		player = e.getPlayer();
		partialTicks = e.getPartialTicks();
		RayTraceResult eyeTrace = ForgeHooks.rayTraceEyes(player, MAXTRACE);
		currentBlock = getSelectedBlockPos(eyeTrace);
		calculatePartialCoords();
    	RenderTools.setPartialCoords(relX, relY, relZ);
    	RenderTools.setRenderDist(drawDist);
	}
	
	@SubscribeEvent
	public void onServerTick(ClientTickEvent e) {
		renderSync = true;
	}
	
	@SubscribeEvent
	public void onWorldRenderFinish(RenderWorldLastEvent e) {
		if (renderSync) {
			updateDynamicVBOs();
			renderSync = false;
		}
		renderPatterns();
	}
	
	@SubscribeEvent
	public void onClientQuit(ClientDisconnectionFromServerEvent	e) {
		renderSync = true;
	}
	
	private static String getWorldId() {
		BlockPos worldSpawn = new BlockPos( player.getEntityWorld().getWorldInfo().getSpawnX(),
				player.getEntityWorld().getWorldInfo().getSpawnY(),
				player.getEntityWorld().getWorldInfo().getSpawnZ());
		return player.getEntityWorld().getWorldInfo().getWorldName() + worldSpawn.getX() + worldSpawn.getY() + worldSpawn.getZ();
	}
	
	@SubscribeEvent
	public void onClientSave(WorldEvent.Save e) {
		try {
			saveGuides(worldId);
		}
		catch (NullPointerException ex) {
			ex.printStackTrace();
		} 
		catch (IOException e1) {
			e1.printStackTrace();
		} 

	}
	
	@SubscribeEvent
	public void onClientJoin(PlayerEvent.PlayerLoggedInEvent e) {
		player = e.player;
		worldId = getWorldId();
		try {
			printToChat(Reference.NAME + " " + Reference.VERSION + " loaded successfully.");
			printToChat("Loading save " + worldId + ".guide");
			loadGuides(worldId);
		}
		catch (IOException e1) {
			e1.printStackTrace();
			printToChat("Failed to load save.");
		}
	}
	
	public static void saveGuides(String world) throws IOException {
		if (world == null) 
			return;
		File saveFile = new File(SAVEPATH + world + ".guide");
		saveFile.getParentFile().mkdir();
		saveFile.createNewFile();
		FileWriter writer = new FileWriter(saveFile);

		GsonBuilder gson = new GsonBuilder();
		gson.setPrettyPrinting();
		Gson json = gson.create();
		
		for (Guide g : guideList) {
			String guideJson = json.toJson(g);
			writer.write("$" + g.getClass().getName() + "<");
			writer.write(guideJson);
			writer.write(">");
		}
		writer.close();
	}
	
	public static void loadGuides(String world) throws IOException {
		guideList.clear();
		try {
			File saveFile = new File(SAVEPATH + world + ".guide");
			FileReader save = new FileReader(saveFile);
			char nextChar;
			String json = new String();
			Gson gson = new Gson();
			String className = new String();
			int eof = 0;
			
			do {
				nextChar = (char) save.read();
				if (nextChar == '$') {
					json = new String();
					className = new String();
					do {
						nextChar = (char) save.read();
						if (nextChar != '<')
							className = className + nextChar;
					}
					while (nextChar != '<');
					do {
						nextChar = (char) save.read();
						if (nextChar != '>')
							json = json + nextChar;
					}
					while (nextChar != '>');
					System.out.println(className);
					System.out.println(json);
					
					Class _class = getObjectClass(className);
					try {
						Guide guide = (Guide) gson.fromJson(json, _class);
						guideList.add(guide);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			} while (nextChar != (char) -1);
			save.close();
		} catch (ClassNotFoundException e) {
			throw new IOException();
		}
	}
	
	public static Class getObjectClass(String className) throws ClassNotFoundException {
	    return Class.forName(className);
    }

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void onEvent(KeyInputEvent event) {
		
		
		//placement key (R)
		if(keyBinds.get("place").isPressed()) {
			registerGuide(generateGuide(currentBlock));
			settingsGui.updateScreen();
			randomizeColors();
		}
		
		//menu key (G)
		if(keyBinds.get("menu").isPressed()) {
			Minecraft.getMinecraft().displayGuiScreen(settingsGui);
			
		}
	}
	
	private void randomizeColors() {
		double hue = 2D * Math.PI * Math.random();
		//double hue = 0;
		this.red = (float) Math.max(0, Math.min(1, (float)Math.sin(hue) + 0.5));
		this.green = (float) Math.max(0, Math.min(1, (float)Math.sin(hue - 2D/3 * Math.PI) + 0.5));
		this.blue = (float) Math.max(0, Math.min(1, (float)Math.sin(hue - 4D/3 * Math.PI) + 0.5));
	}

	public static void printToChat(String s) {
		player.sendMessage(new TextComponentString("[PrecBuild] " + s).setStyle(CmdHelp.style));
	}

	public static enum FadeMode{
		HARD_CULL,
		ALPHA_FADE,
		SIZE_FADE,
		ALPHA_SIZE_FADE,
		INVERSE_CULL
	}
	
	private static void calculatePartialCoords() {
		relX = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double)partialTicks;
		relY = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double)partialTicks;
    	relZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double)partialTicks;
	}
	
	//converts rayTraceResult to BlockPos
	public static BlockPos getSelectedBlockPos(RayTraceResult movingObjectPositionIn) {
		BlockPos blockpos = new BlockPos(player.posX, player.posY, player.posZ);
		try {
			blockpos = movingObjectPositionIn.getBlockPos();
		}
		catch (NullPointerException ex) {
			Vec3d far = player.getLookVec().scale(MAXTRACE).add(player.getPositionVector());
			blockpos = new BlockPos(far);
		}
		return blockpos;
	}

	public static Guide getSelectedGuide() {
		try {
			return guideList.get(settingsGui.getSelectedIndex());
		}
		catch (Exception ex) {
			return null;
		}
	}
	
	public static void selectGuide(int index) {
		for (Guide g : guideList) {
			g.selected = false;
		}
		try {
			guideList.get(index).selected = true;
			settingsGui.selectGuide(index);
			
		}
		catch (Exception ex) {
		}
		
	}
	
	public static void selectGuide(String name) {
		try {
			for (int i = 0; i < guideList.size(); i++) {
				if (guideList.get(i).getName().compareTo(name) == 0) {
					selectGuide(i);
					GuidesMod.printToChat("Selected guide " + guideList.get(i).getName());
				}
			}
		}
		catch (IndexOutOfBoundsException ex) {
			GuidesMod.printToChat("Failed: no active guides!");
		}
	}
	
	final static double RAYTRACE_STEP_SIZE = 0.2;
	
	/*
	 * selects the closest guide under the crosshair
	 */
	public static void selectGuideLook() {
		Vec3d lookVec = player.getLookVec();
		Vec3d start = player.getPositionEyes(partialTicks);
		Vec3d testPoint;
		boolean flag = false;
		int id;
		
		for (testPoint = new Vec3d(start.x, start.y, start.z);
				testPoint.distanceTo(start) < MAXTRACE;
				testPoint = testPoint.add(lookVec.scale(RAYTRACE_STEP_SIZE))) {
			
			//GHelp.printToChat(new BlockPos(testPoint).toString());
			
			for (id = 0; id < guideList.size(); id++) {
				if (guideList.get(id).hasBlockPos(new BlockPos(testPoint))){
					selectGuide(id);
					flag = true;
					GuidesMod.printToChat("Selected guide " + guideList.get(id).getName());
					return;
				}
			}
		}
		if (!flag)
			GuidesMod.printToChat("Failed: no guides in sight!");
	}
	
	public static int getGuideCount() {
		return guideList.size();
	}
	
	public static Guide generateGuide(BlockPos origin) {
		return settingsGui.modes.getCurrentMode().getGuide(origin, red, green, blue, alpha);
	}
	
	
	public static Mode getMode(int id) {
		return settingsGui.getMode(id);
	}
	
	//iterates through the list of patterns and calls their renderer if they are visible
	public static void renderPatterns() {
		for (int i = 0; i < guideList.size(); i++) {
			if (guideList.get(i).visible) {
				
				//guideList.get(i).renderPattern();
				guideList.get(i).renderPattern();
			}
		}
		settingsGui.renderEvent();
	}

	public static RayTraceResult rayTrace(BlockPos pos, Vec3d start, Vec3d end, AxisAlignedBB boundingBox)
    {
        Vec3d vec3d = start.subtract((double)pos.getX(), (double)pos.getY(), (double)pos.getZ());
        Vec3d vec3d1 = end.subtract((double)pos.getX(), (double)pos.getY(), (double)pos.getZ());
        RayTraceResult raytraceresult = boundingBox.calculateIntercept(vec3d, vec3d1);
        return raytraceresult == null ? null : new RayTraceResult(raytraceresult.hitVec.addVector((double)pos.getX(), (double)pos.getY(), (double)pos.getZ()), raytraceresult.sideHit, pos);
    }
	
	@EventHandler
	public static void init(FMLInitializationEvent event) {
		
		proxy.init(event);
	}
	
	@EventHandler
	public static void postInit(FMLPostInitializationEvent event) {
		
		proxy.postInit(event);
	}
	
}


