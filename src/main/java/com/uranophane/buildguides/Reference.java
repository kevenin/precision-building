package com.uranophane.buildguides;

public class Reference {

	public static final String 
			MODID = "buildguides", 
			NAME = "Precision Building", 
			VERSION = "0.5.0",
			CLIENTPROXY = "com.uranophane.buildguides.proxy.ClientProxy",
			COMMONPROXY = "com.uranophane.buildguides.guidesmod.proxy.CommonProxy";
}
