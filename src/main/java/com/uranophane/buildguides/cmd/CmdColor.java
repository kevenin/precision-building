package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.GuidesMod;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CmdColor extends CommandBase{
	Style style = new Style().setItalic(true).setColor(TextFormatting.GOLD);
	public CmdColor() {
	}

	@Override
	public String getName() {
		return ".color";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.color <red> <green> <blue> <alpha>";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			GuidesMod.red = (float) parseDouble(args[0]);
			GuidesMod.green = (float) parseDouble(args[1]);
			GuidesMod.blue = (float) parseDouble(args[2]);
			GuidesMod.alpha = (float) parseDouble(args[3]);
			GuidesMod.getSelectedGuide().red = (float) parseDouble(args[0]);
			GuidesMod.getSelectedGuide().green = (float) parseDouble(args[1]);
			GuidesMod.getSelectedGuide().blue = (float) parseDouble(args[2]);
			GuidesMod.getSelectedGuide().alpha = (float) parseDouble(args[3]);
			GuidesMod.getSelectedGuide().updateCheapVBO();
		}
		catch(Exception ex) {
			sender.sendMessage(
					new TextComponentString(getUsage(sender)).setStyle(style)
					);
		}
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
