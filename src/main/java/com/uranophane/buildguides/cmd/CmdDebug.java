package com.uranophane.buildguides.cmd;

import javax.vecmath.Vector4d;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.util.TransformGuide;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.Vec3d;

public class CmdDebug extends CommandBase {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return ".debug";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		Vec3d testVec = new Vec3d(5, 0, 5);
		TransformGuide testTf = new TransformGuide();
		testTf.addRotY(Math.PI, new Vec3d(0,0,0)).calculateTf();
		
		GuidesMod.printToChat("Testing:" + testVec.toString());
		GuidesMod.printToChat("Matrix:\n" + testTf.toString());
		
		testVec = testTf.transform(testVec);
		GuidesMod.printToChat("Transformed:" + testVec.toString());
	}

}
