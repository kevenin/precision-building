package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.proxy.ClientProxy;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CmdHelp extends CommandBase {
	public static Style style = new Style().setItalic(true).setColor(TextFormatting.GOLD);
	public CmdHelp() {	
	}

	@Override
	public String getName() {
		return ".help";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.help [command]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			String command = "." + args[0];
			for (CommandBase cmd : ClientProxy.commands) {
				if (cmd.getName().contentEquals(command)) {
				sender.sendMessage(
					new TextComponentString(cmd.getUsage(sender)).setStyle(style)
					);
				}
			}
		}
		catch (IndexOutOfBoundsException ex) {
			sender.sendMessage(
					new TextComponentString("Use /.help <command> for more information:").setStyle(style)
					);
			for (CommandBase cmd : ClientProxy.commands) {
				sender.sendMessage(
					new TextComponentString("   /" + cmd.getName()).setStyle(style)
					);
			}
		}
	}
	
	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
