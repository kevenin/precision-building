package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.GuidesMod;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class CmdPlace extends CommandBase{

	public CmdPlace() {
	}

	@Override
	public String getName() {
		return ".place";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.place [x] [y] [z]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			int x = (int) CommandBase.parseCoordinate(sender.getPosition().getX(), args[0], false).getResult();
			int y = (int) CommandBase.parseCoordinate(sender.getPosition().getY(), args[1], false).getResult();
			int z = (int) CommandBase.parseCoordinate(sender.getPosition().getZ(), args[2], false).getResult();
			BlockPos origin = new BlockPos(x, y, z);
			GuidesMod.registerGuide(GuidesMod.generateGuide(origin));
		}
		catch(IndexOutOfBoundsException ex) {
			GuidesMod.registerGuide(GuidesMod.generateGuide(GuidesMod.currentBlock));
		}
	}
	
	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
