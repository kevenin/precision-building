package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;

public class CmdSel extends CommandBase {
	Style style = new Style().setItalic(true).setColor(TextFormatting.GOLD);
	public CmdSel(){
	}

	@Override
	public String getName() {
		return ".select";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.select [name]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			GuidesMod.selectGuide(args[0].join(" ", args));
		}
		catch (Exception ex) {
		}
		try {
			GuidesMod.selectGuideLook();
		}
		catch (Exception ex) {
		}
	}
	
	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
