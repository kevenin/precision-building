package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

public class CmdVis extends CommandBase{

	public CmdVis() {
	}
	
	@Override
	public String getName() {
		return ".visible";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.visible [1/0/all]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		Guide g = GuidesMod.getSelectedGuide();
		try {
			switch (args[0]) {
			case "0":
				g.visible = false;
				break;
			case "1":
				g.visible = true;
				break;
			case "all":
				for (Guide guide : GuidesMod.guideList) {
					guide.toggleVis();
				}
				break;
			default: 
			}
		}
		catch(Exception ex) {
			g.toggleVis();
		}
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
