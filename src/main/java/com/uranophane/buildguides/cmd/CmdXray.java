package com.uranophane.buildguides.cmd;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CmdXray extends CommandBase{

	public CmdXray() {
	}
	
	@Override
	public String getName() {
		return ".xray";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/.xray [1/0/all]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		Guide g = GuidesMod.getSelectedGuide();
		try {
			switch (args[0]) {
			case "0":
				g.xray = false;
				break;
			case "1":
				g.xray = true;
				break;
			case "all":
				for (Guide guide : GuidesMod.guideList) {
					guide.toggleXray();
				}
				break;
			default: 
			}
		}
		catch(Exception ex) {
			g.toggleXray();
		}
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
}
