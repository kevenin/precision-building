package com.uranophane.buildguides.gui;

import com.uranophane.buildguides.GuidesMod;

import net.minecraft.client.gui.GuiPageButtonList.GuiResponder;

public class ColorResponder implements GuiResponder {

	public ColorResponder() {
		super();
	}
	
	@Override
	public void setEntryValue(int id, boolean value) {
		

	}

	@Override
	public void setEntryValue(int id, float value) {
		switch (id) {
		case 500:	//red
			GuidesMod.red = value;
			break;
		case 501:	//green
			GuidesMod.green = value;
			break;
		case 502:	//blue
			GuidesMod.blue = value;
			break;
		case 503:	//alpha
			GuidesMod.alpha = value;
			break;
		case 504:	//render dist
			GuidesMod.drawDist = value;
			break;
		}

	}

	@Override
	public void setEntryValue(int id, String value) {
		// TODO Auto-generated method stub

	}

}
