package com.uranophane.buildguides.gui;

import net.minecraft.client.gui.GuiPageButtonList.GuiResponder;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSlider.FormatHelper;

public class ColorSlider extends GuiSlider{
	
	public ColorSlider(GuiResponder guiResponder, int idIn, int x, int y, String nameIn, float minIn, float maxIn,
			float defaultValue) {
		super(guiResponder, idIn, x, y, nameIn, minIn, maxIn, defaultValue, new roundFormatter());
	}
	

}

class roundFormatter implements FormatHelper{	//rounds to 2 decimal points

	@Override
	public String getText(int id, String name, float value) {
		//finds index of the period and gets the part before it + 2 spaces. Checks to make sure index doesn't overflow
		return name + ": " + Float.toString(value).substring(0, Math.min(Float.toString(value).indexOf('.') + 3, Float.toString(value).length() - 1));
	}
	
}