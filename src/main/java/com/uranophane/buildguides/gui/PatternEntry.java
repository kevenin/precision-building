package com.uranophane.buildguides.gui;

import java.util.ArrayList;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.modes.ModeObjController;
import com.uranophane.buildguides.util.ColorManip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiListExtended.IGuiListEntry;
import net.minecraft.util.math.Vec3i;

public class PatternEntry implements IGuiListEntry{

	private int id;
	private int modeId;
	ArrayList<Float> params;
	Vec3i origin;
	private PatternList parent;
	
	private final Minecraft client;
	private Guide guide;
	
	public PatternEntry(int id, PatternList parent, Guide guide) {
		this.id = id;
		this.modeId = guide.modeId;
		this.origin = guide.origin;
		this.guide = guide;
		this.parent = parent;
		this.client = Minecraft.getMinecraft();
	}
	
	public int getIndex() {
		return this.id;
	}
	
	public int getModeId() {
		return this.modeId;
	}
	
	public Vec3i getOrigin() {
		return this.origin;
	}
	
	@Override
	public void updatePosition(int p_192633_1_, int p_192633_2_, int p_192633_3_, float p_192633_4_) {
	}

	@Override
	public void drawEntry(int slotIndex, int x, int y, int listWidth, int slotHeight, int mouseX, int mouseY,
			boolean isSelected, float partialTicks) {
		
		String s1 = ModeObjController.getModeName(this.getModeId());
		String s2 = this.guide.getDisplayParam();
		String s3 = Integer.toString(origin.getX()) + ", " + 
					Integer.toString(origin.getY()) + ", " + 
					Integer.toString(origin.getZ());
		this.client.fontRenderer.drawString("o", x + 4 + 3, y + 1, ColorManip.RGBftoInt(guide.red, guide.green, guide.blue));
		this.client.fontRenderer.drawString(s1, x + 4 + 12, y + 1, 16777215);	//draw string 1 on top
		this.client.fontRenderer.drawString(s2, x + 4 + 3, y + this.client.fontRenderer.FONT_HEIGHT + 3, 8421504);	//draw string 2 1 row down
		this.client.fontRenderer.drawString(s3, x + 4 + 3, y + 2 * (this.client.fontRenderer.FONT_HEIGHT + 3), 8421504);	//draw string 3 2 rows down
		
	}

	@Override
	public boolean mousePressed(int slotIndex, int mouseX, int mouseY, int mouseEvent, int relativeX, int relativeY) {

		parent.selectedIndex = slotIndex;
		return false;
	}

	@Override
	public void mouseReleased(int slotIndex, int x, int y, int mouseEvent, int relativeX, int relativeY) {
	}


	
}
