package com.uranophane.buildguides.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.math.Vec3d;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.lwjgl.input.Keyboard;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.cmd.CmdHelp;
import com.uranophane.buildguides.gui.buttons.ColorSlider;
import com.uranophane.buildguides.gui.buttons.ModeButton;
import com.uranophane.buildguides.gui.modes.Bezier;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.gui.modes.ModeProxy;
import com.uranophane.buildguides.gui.textfields.LabelledTextField;
import com.uranophane.buildguides.gui.textfields.SignedFloatField;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideList;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.util.NumberManip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;

public class SettingsGui extends GuiScreen{

	private static final int MODEBUTTON_ID_OFFSET = 1000;
	private static final int SLIDER_RENDER_DIST = 504;
	private static final int SLIDER_ALPHA = 503;
	private static final int SLIDER_BLUE = 502;
	private static final int SLIDER_GREEN = 501;
	private static final int SLIDER_RED = 500;
	private static final int BUTTON_VIS = 404;
	private static final int BUTTON_RESET = 403;
	private static final int BUTTON_SAVE = 402;
	private static final int BUTTON_XFORM = 405;
	private static final int BUTTON_DELETE = 400;
	private static final int BUTTON_LOAD = 401;
	private static final int BUTTON_CHEAP = 406;
	
	private String title = "Guide Settings";
	protected int modeId = 0;
	protected int currentModeButtonId = MODEBUTTON_ID_OFFSET;
	public ArrayList<LabelledTextField> modeFieldList = new ArrayList();			// mode-specific text fields
	protected HashMap<Integer, LabelledTextField> commonFieldList = new HashMap();	// universal text fields
	protected HashMap<Integer, LabelledTextField> fieldList = new HashMap();			// all text fields
	
	protected ArrayList<GuiButton> modeButtonList = new ArrayList();
	protected HashMap<Integer, String> controlButtons = new HashMap();
	protected UiGuideList guideListMenu;
	protected final ColorResponder responder = new ColorResponder();
	public final ModeProxy modes = new ModeProxy(this);

	public static final int modeButtonWidth = 100;

	private double rotAng;
	private TransformGui xformGui;
	
	public int getModeId() {
		return modeId;
	}
	
	public int getSelectedIndex() {
		try {
			return guideListMenu.getSelectedIndex();
		}
		catch (Exception ex) {
			return 0;
		}
	}
	
	public SettingsGui() {
		//the pattern list
		this.mc = Minecraft.getMinecraft();
		this.guideListMenu = new UiGuideList(mc, this, this.width * 2 - 160, height, 20, height-48, 36);
		this.xformGui = new TransformGui(this);
	}
	
	private void registerControlButtons() {
		controlButtons.put(BUTTON_XFORM, "Transform");
		controlButtons.put(BUTTON_LOAD, "Load <<");
		controlButtons.put(BUTTON_SAVE, "Overwrite >>");
		controlButtons.put(BUTTON_VIS, "Visibility");
		controlButtons.put(BUTTON_RESET, "Clear Params");
		controlButtons.put(BUTTON_CHEAP, "Toggle Sketch");
	}
	
	@Override
	public void initGui() {
		super.initGui();
		modes.setFR(fontRenderer);
		
	// all the lists
		this.guideListMenu.width = this.width * 2 - 160;
		this.guideListMenu.height = height;
		this.guideListMenu.top = 20;
		this.guideListMenu.bottom = height-48;
		this.guideListMenu.left = 0; //this.width - 160;
		this.guideListMenu.right = this.width;
		this.guideListMenu.refreshList();
		
		this.buttonList.clear();
		this.fieldList.clear();
		
		rotAng = 0;
		
		//the static buttons
		registerControlButtons();
		addCommonButtons();
		addCommonTextBoxes();
		updateUIMode(currentModeButtonId);
	}
	
	/**
	 * Makes a master list of all textboxes to render 
	 */
	private void mergeFieldLists() {
		this.fieldList.putAll(commonFieldList);
		for(int i = 0; i < modeFieldList.size(); i++) {
			this.fieldList.put(i,modeFieldList.get(i));
		}
	}
	
	private void drawAllTextBoxes() {
		// iterate over all the text boxes and draw them
		for (LabelledTextField f : fieldList.values()) {
			f.drawWithLabel(this);
		}
	}
	
	private void addCommonTextBoxes() {
	}
	
	
	private void allTextBoxMouse(int mouseX, int mouseY, int mouseButton) {
		for (LabelledTextField f : fieldList.values()) {
			f.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}
	
	private void commonTextBoxCollect() {
	}

	
	
	private void addCommonButtons() { //all mode buttons have ids > 1000
		/*
		[ RED  ] [GREEN ] [ BLUE ] button default width = 150
		[  RD  ] [ALPHA ] [DELETE] button default height = 20
		*/
		
		//Tile mode buttons down in order:
		for (int i = 0; i < modes.getModeCount(); i++) {
			this.buttonList.add(new ModeButton(MODEBUTTON_ID_OFFSET + i, 
				4, 				//4 from left border
				24 * i + 20, 	//goes up in multiples of 24
				modeButtonWidth, 20, 
				modes.getModeName(i)) );
		}
		
		//delete button
		this.buttonList.add(new GuiButton(BUTTON_DELETE,
				this.width/2 + 150 - 75 + 4,	//4 from right border
				this.height - 24,	//4 from bottom
				150, 20,
				"Delete Guide"));
		
		// add all the control buttons
		SortedSet<Integer> buttonIds = new TreeSet(controlButtons.keySet());
		int iter = 0;
		for (int id : buttonIds) {
			this.buttonList.add(new GuiButton(id,
					width - guideListMenu.getListWidth() - 80 - 4,
					24 * iter + 20,
					80, 20,
					controlButtons.get(id)));
			iter++;
		}
		
		//color sliders
		this.buttonList.add(new ColorSlider(responder, SLIDER_RED, 
				this.width/2 - 150 - 75 - 4, this.height - 48, 
				"Red", 0F, 1F, GuidesMod.red));
		
		this.buttonList.add(new ColorSlider(responder, SLIDER_GREEN, 
				this.width/2 - 75, this.height - 48, 
				"Green", 0F, 1F, GuidesMod.green));
		
		this.buttonList.add(new ColorSlider(responder, SLIDER_BLUE, 
				this.width/2 + 150 - 75 + 4, this.height - 48, 
				"Blue", 0F, 1F, GuidesMod.blue));
		
		this.buttonList.add(new ColorSlider(responder, SLIDER_ALPHA, 
				this.width/2 - 75, this.height - 24, 
				"Transparency", 0F, 1F, GuidesMod.alpha));
		
		//render dist
		this.buttonList.add(new ColorSlider(responder, SLIDER_RENDER_DIST, 
				this.width/2 - 150 - 75 - 4, this.height - 24, 
				"Draw Distance", 1F, 40F, GuidesMod.drawDist));
	}
	
	@Override
	public void actionPerformed(GuiButton button) {
		modes.collectInput();
		commonTextBoxCollect();
		GuidesMod.updateVBOs();
		try {
			Guide g = GuidesMod.getSelectedGuide();
			switch (button.id) {

				case BUTTON_DELETE:
					if (!GuidesMod.guideList.isEmpty()) {
						GuidesMod.guideList.remove(getSelectedIndex());
						guideListMenu.refreshList();
					}
					break;

				case BUTTON_LOAD:
					if (!GuidesMod.guideList.isEmpty()) {
						// first get the modeID
						// the indices in guideList directly map to those in patternList, this is crucial
						modeId = g.modeId;
						// now with the correct mode object active, tell them to fetch and display parameters
						modes.loadPattern(g);
						GuidesMod.red = g.red;
						GuidesMod.green = g.green;
						GuidesMod.blue = g.blue;
						GuidesMod.alpha = g.alpha;
						updateUIMode(modeId + MODEBUTTON_ID_OFFSET);
					}
					break;

				case BUTTON_SAVE:
					if (!GuidesMod.guideList.isEmpty()) {
						// generate a new pattern to replace the old one
						GuidesMod.guideList.set(getSelectedIndex(), modes.getCurrentMode().getGuide(g.origin, GuidesMod.red, GuidesMod.green, GuidesMod.blue, GuidesMod.alpha));
						guideListMenu.refreshList();
					}
					break;

				case BUTTON_RESET:
					modes.getCurrentMode().defParams();
					updateUI();
					break;

				case BUTTON_XFORM:
					xformGui.loadTransform(GuidesMod.getSelectedGuide().getTransform());
					mc.displayGuiScreen(xformGui);
					break;
					
				case BUTTON_VIS:
					g.toggleVis();
					updateUI();
					break;
					
				case BUTTON_CHEAP:
					GuidesMod.getSelectedGuide().toggleCheap();
					break;
					
			/* Other global buttons go here */
			
			}
		}
		catch (IndexOutOfBoundsException e) {
			
		}
		// 1000+ are mode buttons
		if (button.id >= MODEBUTTON_ID_OFFSET) {
			currentModeButtonId = button.id;
			modeId = currentModeButtonId - MODEBUTTON_ID_OFFSET;
			updateUIMode(button.id);
		}
		// handle a non-mode button press
		modes.paramButtonPress(button.id);
		
	}
	
	
	public void updateUIMode(int buttonId) {	//handles the appearance of mode buttons
		
		//enable other buttons if a mode button is pressed
		if(buttonId >= MODEBUTTON_ID_OFFSET) {
			enableAllButtonsExcept(buttonId);
		}
		
		updateUI();
	
	}
	
	public void updateUI() {
		//remove all non-special buttons (id < 400) 
		
		for (int i = 0; i < this.buttonList.size(); i++) {
			if (this.buttonList.get(i).id < BUTTON_DELETE) {
				this.buttonList.remove(i);
				i--;
			}
		}
		//remove all parameter fields and add back
		this.modeFieldList.clear();
		this.fieldList.clear();
		modes.setFR(fontRenderer);
		modes.setupCurrentUI();
		mergeFieldLists();
		addAllCustomButtons();
	}
	


	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		
		super.drawDefaultBackground();								//background layer

		this.guideListMenu.drawScreen(mouseX, mouseY, partialTicks);	//pattern list box
		
		drawAllTextBoxes();
		for (GuiTextField t : fieldList.values()) {
			if (t.isFocused()) {

				this.drawCenteredString(fontRenderer, "Click anywhere to confirm numerical input.", this.width / 2, this.height - 60, 16777094);
			}
		}
		modes.renderUiEvent();
		
		updateScreen();
		super.drawScreen(mouseX, mouseY, partialTicks);				//buttons layer
		this.drawCenteredString(fontRenderer, this.title, this.width / 2, 8, 16777215);
	}
	
	@Override
	public void keyTyped(char c, int key) throws IOException{
		super.keyTyped(c, key);
		for (LabelledTextField f : fieldList.values()) {
			f.textboxKeyTyped(c, key);
		}
	}
	

	@Override
	public void handleMouseInput() throws IOException
    {
        super.handleMouseInput();
        this.guideListMenu.handleMouseInput();   
    }
	
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.guideListMenu.mouseClicked(mouseX, mouseY, mouseButton);
        allTextBoxMouse(mouseX, mouseY, mouseButton); //send mouse input to all text boxes 
        modes.collectInput();
		commonTextBoxCollect();
        GuidesMod.updateVBOs();
    }
	
	@Override
	public void updateScreen(){
		GuidesMod.selectGuide(guideListMenu.getSelectedIndex());
		if (guideListMenu.isEmpty()) {
			disableButton(BUTTON_LOAD);
			disableButton(BUTTON_SAVE);
			disableButton(BUTTON_VIS);
			disableButton(BUTTON_DELETE);
			disableButton(BUTTON_XFORM);
		} else {
			enableButton(BUTTON_LOAD);
			enableButton(BUTTON_SAVE);
			enableButton(BUTTON_VIS);
			enableButton(BUTTON_DELETE);
			enableButton(BUTTON_XFORM);
		}

	}
	
	/**
	 * Adds an Unsigned Float Field to THIS GUI
	 * @param id 
	 * @param label Name to display
	 * @param x
	 * @param y
	 * @param w Width
	 * @param initVal Initial value
	 * @param maxLen Max length
	 */
	public void addParamField(int id, String label, int x, int y, int w, String initVal, int maxLen) {
		
		//add textbox
		UnsignedFloatField field = new UnsignedFloatField(fontRenderer, id, label,
				modeButtonWidth + x, fontRenderer.FONT_HEIGHT + y + 2, 	//location
				w, initVal, maxLen);							//size
		modeFieldList.add(field);										
	}
	
	/**
	 * Adds a clickable button object to THIS GUI
	 * @param id a unique ID for this button
	 * @param label the text to display on this button
	 * @param x1 starting x
	 * @param y1 starting y
	 * @param x2 ending x
	 * @param y2 ending y
	 */
	public void addButton(int id, String label, int x1, int y1, int x2, int y2) {
		this.modeButtonList.add(new GuiButton(id,
				x1, y1,
				x2 - x1, y2 - y1,
				label));
	}
	
	public void removeButton(int id) {
		for (int i = 0; i < buttonList.size(); i++) {
			if (buttonList.get(i).id == id) {
				this.buttonList.remove(i);
				return;
			}
		}
	}
	
	public void addAllCustomButtons() {
		for (GuiButton button : modeButtonList) {
			this.buttonList.add(button);
		}
		modeButtonList.clear();
	}
	
	public void enableAllButtonsExcept(int id) {
		for (GuiButton button : buttonList) {
			if (button.id != id) {
				button.enabled = true;
			}
			else {
				button.enabled = false;
			}
		}
	}
	
	public void disableButton(int id) {
		for (GuiButton button : buttonList) {
			if (button.id == id && button.enabled) {
				button.enabled = false;
			}
		}
		for (GuiButton button : modeButtonList) {
			if (button.id == id && button.enabled) {
				button.enabled = false;
			}
		}
	}
	
	public void enableButton(int id) {
		for (GuiButton button : buttonList) {
			if (button.id == id) {
				button.enabled = true;
			}
		}
	}
	
	public void selectGuide(int id) {
		try {
			this.guideListMenu.selectIndex(id);
		}
		catch (IndexOutOfBoundsException ex) {}
	}

	public Mode getMode(int id) {
		return this.modes.getMode(id);
	}
	
	public void renderEvent() {
		modes.renderEvent();
		xformGui.renderEvent();
	}
	
}

