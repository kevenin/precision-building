package com.uranophane.buildguides.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;

import javax.annotation.Nullable;

import org.lwjgl.opengl.GL11;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.textfields.LabelledTextField;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.util.RenderTools;
import com.uranophane.buildguides.util.TransformGuide;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class TransformGui extends GuiScreen {
	private static final int BUTTON_W_MARGIN = 84;
	private static final int BUTTON_W = 80;
	private static final int BUTTON_H_MARGIN = 24;
	private static final int BUTTON_H = 20;
	private int MID_X = this.width/2;
	private int MID_Y = this.height/2;
	private static final int BUTTON_SETORIGIN = 590;
	private static final int BUTTON_ROTZ = 591;
	private static final int BUTTON_ROTY = 592;
	private static final int BUTTON_ROTX = 593;
	private static final int BUTTON_MOVZ = 594;
	private static final int BUTTON_MOVY = 595;
	private static final int BUTTON_MOVX = 596;
	private static final int BUTTON_UNDO = 570;
	private static final int BUTTON_CONF = 571;
	private static final int FIELD_ANGLE = 580;
	private static final int FIELD_SHIFT = 581;
	private static final String TITLE = "Transformation";
	private double rotation;
	public Vec3i rotOrigin;
	private double translation;
	private TransformGuide xform = new TransformGuide();
	
	protected SettingsGui gui;
	protected HashMap<Integer, LabelledTextField> fieldList = new HashMap();
	
	private TfMode editMode;

	private enum TfMode{
		X_ROT,
		Y_ROT,
		Z_ROT,
		X_SHIFT,
		Y_SHIFT,
		Z_SHIFT,
		NONE
	}
	
	@Nullable
	public TransformGui(SettingsGui parent) {
		this.mc = Minecraft.getMinecraft();
		this.gui = parent;
		this.editMode = TfMode.NONE;
		this.rotOrigin = new Vec3i(0,0,0);
	}

	@Override
	public void initGui() {
		super.initGui();
		this.buttonList.clear();
		MID_X = this.width/2;
		MID_Y = this.height/2;
		this.editMode = TfMode.NONE;
		
		this.rotation = 0;
		this.translation = 0;
		
		addButtons();
		addFields();
	}
	
	
	private void addFields() {
		this.fieldList.put(FIELD_ANGLE, 
				new LabelledTextField(fontRenderer, FIELD_ANGLE, "Rotation (degrees):", MID_X - 2*BUTTON_W - 20, MID_Y - 40, 80, Double.toString(this.rotation), 6)
				);
		this.fieldList.put(FIELD_SHIFT, 
				new LabelledTextField(fontRenderer, FIELD_ANGLE, "Translation:", MID_X - 2*BUTTON_W - 20, MID_Y + 40, 80, Double.toString(this.translation), 6)
				);
	}
	
	private void drawFields() {
		for (LabelledTextField field : fieldList.values()) {
			field.drawWithLabel(this);
		}
	}
	
	private void parseFields() {
		this.rotation = NumberManip.stringToDouble_s(fieldList.get(FIELD_ANGLE).getText());
		this.translation = NumberManip.stringToDouble_s(fieldList.get(FIELD_SHIFT).getText());
	}

	private void addButtons() {
		// X-rotate button
		this.buttonList.add(new GuiButton(BUTTON_ROTX,
				//location
				MID_X - BUTTON_W_MARGIN, 
				MID_Y - 40, 	
				BUTTON_W, BUTTON_H,
				"Rotate Pitch"));
		
		// Y-rotate button
		this.buttonList.add(new GuiButton(BUTTON_ROTY,
				//location
				MID_X, 
				MID_Y - 40, 	
				BUTTON_W, BUTTON_H,
				"Rotate Yaw"));
		
		// Z-rotate button
		this.buttonList.add(new GuiButton(BUTTON_ROTZ,
				//location
				MID_X + BUTTON_W_MARGIN, 
				MID_Y - 40, 	
				BUTTON_W, BUTTON_H,
				"Rotate Roll"));
		
		// set origin button
		this.buttonList.add(new GuiButton(BUTTON_SETORIGIN,
				MID_X - 2 * BUTTON_W_MARGIN, 
				MID_Y - 100, 	//location
				2 * BUTTON_W, BUTTON_H,
				"Set Rotation Origin"));
		
		// undo button
		this.buttonList.add(new GuiButton(BUTTON_UNDO,
				MID_X, 
				MID_Y - 100, 	//location
				BUTTON_W, BUTTON_H,
				"Undo"));
		
		// consolidate button
		this.buttonList.add(new GuiButton(BUTTON_CONF,
				MID_X + BUTTON_W_MARGIN, 
				MID_Y - 100, 	//location
				BUTTON_W, BUTTON_H,
				"Merge All"));
				
		// X-shift button
		this.buttonList.add(new GuiButton(BUTTON_MOVX,
				//location
				MID_X - BUTTON_W_MARGIN, 
				MID_Y + 40, 	
				BUTTON_W, BUTTON_H,
				"Shift East"));
		
		// Y-shift button
		this.buttonList.add(new GuiButton(BUTTON_MOVY,
				//location
				MID_X, 
				MID_Y + 40, 	
				BUTTON_W, BUTTON_H,
				"Shift Up"));
		
		// Z-rotate button
		this.buttonList.add(new GuiButton(BUTTON_MOVZ,
				//location
				MID_X + BUTTON_W_MARGIN, 
				MID_Y + 40, 	
				BUTTON_W, BUTTON_H,
				"Shift North"));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawDefaultBackground();								//background layer
		drawAllTextBoxes();
		if (GuidesMod.getSelectedGuide() == null) {
			disableButton(BUTTON_ROTX);
			disableButton(BUTTON_ROTY);
			disableButton(BUTTON_ROTZ);
			disableButton(BUTTON_SETORIGIN);
			disableButton(BUTTON_MOVX);
			disableButton(BUTTON_MOVY);
			disableButton(BUTTON_MOVZ);
		} else {
			enableButton(BUTTON_ROTX);
			enableButton(BUTTON_ROTY);
			enableButton(BUTTON_ROTZ);
			enableButton(BUTTON_SETORIGIN);
			enableButton(BUTTON_MOVX);
			enableButton(BUTTON_MOVY);
			enableButton(BUTTON_MOVZ);
		} 
		
		if (this.xform.isEmpty()) {
			disableButton(BUTTON_UNDO);
			disableButton(BUTTON_CONF);
		} else {
			enableButton(BUTTON_UNDO);
			enableButton(BUTTON_CONF);
		}
		
		super.drawScreen(mouseX, mouseY, partialTicks);
		drawString(fontRenderer, "History:", 20, 80, 0xFFFFDD);
		ArrayList<String> hist = xform.getTfRecord();
		for (int i = 0; i < hist.size(); i++) {
			drawString(fontRenderer, hist.get(i), 20, 100 + i * (fontRenderer.FONT_HEIGHT + 2), 0xFFFFDD);
		}
	}
	
	private void drawAllTextBoxes() {
		// iterate over all the text boxes and draw them
		for (LabelledTextField f : fieldList.values()) {
			f.drawWithLabel(this);
		}
	}

	public void actionPerformed(GuiButton button) {
		try {
			parseFields();
			Guide g = GuidesMod.getSelectedGuide();
			switch (button.id) {
			case BUTTON_SETORIGIN:
				// use the same frame of reference as the pattern:
				this.rotOrigin = GuidesMod.currentBlock.subtract(g.origin);
				xform.origin = new Vec3d(this.rotOrigin);
				break;
			case BUTTON_ROTX:
				if (rotation != 0)
					xform.addRotX(Math.toRadians(rotation), new Vec3d(rotOrigin));
				break;
			case BUTTON_ROTY:
				if (rotation != 0)
					xform.addRotY(Math.toRadians(rotation), new Vec3d(rotOrigin));
				break;
			case BUTTON_ROTZ:
				if (rotation != 0)
					xform.addRotZ(Math.toRadians(rotation), new Vec3d(rotOrigin));
				break;
			case BUTTON_MOVX:
				xform.addShift(translation, 0, 0);
				break;
			case BUTTON_MOVY:
				xform.addShift(0, translation, 0);
				break;
			case BUTTON_MOVZ:
				xform.addShift(0, 0, translation);
				break;
			case BUTTON_UNDO:
				xform.undoXform();
				break;
			case BUTTON_CONF:
				GuidesMod.getSelectedGuide().mergeTransform();
			}
			GuidesMod.getSelectedGuide().doTransform();
		}
		catch (NullPointerException ex) {
			this.disableAllButtons();
		}
	}
	
	@Override
	public void keyTyped(char c, int key) throws IOException{
		super.keyTyped(c, key);
		for (LabelledTextField f : fieldList.values()) {
			f.textboxKeyTyped(c, key);
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        allTextBoxMouse(mouseX, mouseY, mouseButton);
        parseFields();
    }
	
	/**
	 * Sends mouse input to every text field
	 * @param mouseX
	 * @param mouseY
	 * @param mouseButton
	 */
	private void allTextBoxMouse(int mouseX, int mouseY, int mouseButton) {
		for (LabelledTextField f : fieldList.values()) {
			f.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}

	public void renderEvent() {
		switch (editMode) {
		case NONE:
			break;
		case X_ROT:
			break;
		case X_SHIFT:
			break;
		case Y_ROT:
			break;
		case Y_SHIFT:
			break;
		case Z_ROT:
			break;
		case Z_SHIFT:
			break;
		default:
			break;
		
		}
	}
	
	private void disableAllButtons() {
		for (GuiButton button : buttonList) {
			button.enabled = false;
		}
	}
	
	private void disableButton(int id) {
		for (GuiButton button : buttonList) {
			if (button.id == id && button.enabled) {
				button.enabled = false;
			}
		}
	}
	
	private void enableButton(int id) {
		for (GuiButton button : buttonList) {
			if (button.id == id && !button.enabled) {
				button.enabled = true;
			}
		}
	}
	
	public TransformGuide getTransform() {
		return this.xform;
	}
	
	public void loadTransform(TransformGuide tf) {
		this.xform = tf;
	}
}
