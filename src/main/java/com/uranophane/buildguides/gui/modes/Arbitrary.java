package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.HashSet;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideArb;
import com.uranophane.buildguides.guides.GuideTriangle;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Arbitrary extends Mode{
	private static final int BUTTON_START 	= 0;
	private static final int BUTTON_CANCEL	= 1;
	private boolean drawing;
	private Vec3i point1;
	private Vec3i point2;
	private HashSet<BlockPos> pattern = new HashSet();
	private PaintMode editMode = PaintMode.NONE;
	
	private enum PaintMode{
		NONE,
		START,
		END;
	}
	
	public Arbitrary(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public String getName() {
		return "Arbitrary";
	}

	@Override
	public void defParams() {
		drawing = false;
		editMode = PaintMode.NONE;
	}

	@Override
	void setupUI() {
		gui.addButton(BUTTON_START, "Set Pos. 1", gui.modeButtonWidth + 10, 100, 200, 120);
		gui.addButton(BUTTON_CANCEL, "Set Pos. 2", gui.modeButtonWidth + 10, 76, 200, 96);
	}
	
	@Override
	void handleButtonInput(int buttonId) {
		switch (buttonId) {
		case BUTTON_START:
			point1 = GuidesMod.currentBlock.toImmutable();
			break;
		case BUTTON_CANCEL:
			point2 = GuidesMod.currentBlock.toImmutable();
			break;
		}
	}
	

	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideArb guide = new GuideArb(new BlockPos(point1), red, green, blue, alpha, this);
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		
	}
	

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=false)
	public void onEvent(KeyInputEvent event) {
		
		//special action key (E)
		if(GuidesMod.keyBinds.get("special").isPressed()) {
			switch (editMode) {
			case END:
				point2 = GuidesMod.currentBlock.toImmutable();
				editMode = PaintMode.START;
				break;
			case NONE:
				break;
			case START:
				point1 = GuidesMod.currentBlock.toImmutable();
				editMode = PaintMode.END;
				break;
			default:
				break;
			
			}
		}
	} 
}
