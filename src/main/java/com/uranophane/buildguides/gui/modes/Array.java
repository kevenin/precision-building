package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.TransformGui;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideArray;
import com.uranophane.buildguides.guides.GuideTriangle;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.util.TransformGuide;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class Array extends Mode {
	Guide duplicand;
	TransformGuide propagate;
	private int reps = 0;
	private TransformGui xformUi;
	private static final int BUTTON_SETGUIDE = 0;
	private static final int BUTTON_XFORM	 = 1;
	private static final int REPS = 3;

	public Array(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public String getName() {
		return "Array";
	}

	@Override
	public void defParams() {
		duplicand = null;
		propagate = new TransformGuide();
		xformUi = new TransformGui(gui);
		reps = 0;
	}

	@Override
	void setupUI() {
		gui.addParamField(REPS, "Repetitions", 10, 40, 50, Integer.toString(reps), 5);
		gui.addButton(BUTTON_SETGUIDE, "Set Selected Guide as Stamp", gui.modeButtonWidth + 10, 120, 300, 140);
		gui.addButton(BUTTON_XFORM, "Edit Incremental Transformation", gui.modeButtonWidth + 10, 76, 300, 96);
		
		if (GuidesMod.getSelectedGuide() == null)
			gui.disableButton(BUTTON_SETGUIDE);
		else
			gui.enableButton(BUTTON_SETGUIDE);
	}
	@Override
	void handleButtonInput(int buttonId) {
		switch (buttonId) {
		case BUTTON_SETGUIDE:
			this.duplicand = GuidesMod.getSelectedGuide();
			//this.tf.clear();
			break;
		case BUTTON_XFORM:
			xformUi.loadTransform(this.propagate);
			Minecraft.getMinecraft().displayGuiScreen(xformUi);
			break;
		}
		gui.updateUI();
	}

	@Override
	void collectInput() {
		reps = Integer.parseUnsignedInt(getParamFieldById(REPS).getText());
	}


	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideArray guide = new GuideArray(origin, red, green, blue, alpha, this);
		guide.reps = this.reps;
		guide.duplicand = new ArrayList(this.duplicand.rawCoords);
		guide.propagate = this.propagate.clone();
		guide.initializeInstance();
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideArray) {
			GuideArray g = (GuideArray) guide;
			this.reps = g.reps;
			this.duplicand = null;
			this.propagate = g.propagate.clone();
		}
	}
	
	@Override
	public void renderUiEvent() {
		if (this.duplicand != null)
			gui.drawString(fr, "Current stamp: " + duplicand.getName(), 114, 144, 0xFFFFDD);
		else
			gui.drawString(fr, "No stamp guide set", 114, 144, 0xFF4444);
		if (this.propagate.isEmpty())
			gui.drawString(fr, "No transform set", 114, 100, 0xFF4444);
	}

}
