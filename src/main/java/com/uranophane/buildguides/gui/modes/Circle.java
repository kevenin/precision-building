package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideCircle;
import com.uranophane.buildguides.guides.GuideEllipsoid;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Circle extends Mode{
	private final int RADIUS = 0;	//id for radius parameter
	
	private double radius = 0D;
	
	public Circle(SettingsGui parent) {
		super(parent);
		defParams();
	}
	
	@Override
	public
	String getName() {
		return "Circle";
	}

	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add("Radius");
		return paramNames;
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(RADIUS, getParamNames().get(RADIUS), 10, 40, 50, Double.toString(radius), 7);
	}

	@Override
	void collectInput() {
		radius = NumberManip.stringToDouble_s(gui.modeFieldList.get(RADIUS).getText());
	}


	

	@Override
	public
	void defParams() {
		radius = 0D;
	}


	@Override
	void displayParams() {
		gui.modeFieldList.get(RADIUS).setText(Double.toString(radius));
	
	}

	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideCircle guide = new GuideCircle(origin, red, green, blue, alpha, this);
		guide.radius = radius;
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideCircle) {
			GuideCircle circleGuide = (GuideCircle) guide;
			this.radius = circleGuide.radius;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}

}

