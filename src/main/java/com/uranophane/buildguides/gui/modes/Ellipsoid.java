package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.guides.GuideEllipsoid;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Ellipsoid extends Mode{
	private static final int X_RAD = 0;	//ids for directional radius parameters
	private static final int Y_RAD = 1;
	private static final int Z_RAD = 2;
	
	private double x_radius = 0D;
	private double y_radius = 0D;
	private double z_radius = 0D;
	
	public Ellipsoid(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public
	String getName() {
		return "Ellipsoid";
	}

	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add(X_RAD, "X-Radius");
		paramNames.add(Y_RAD, "Y-Radius");
		paramNames.add(Z_RAD, "Z-Radius");
		return paramNames;
	}

	@Override
	public
	void defParams() {
		x_radius = 0D;
		y_radius = 0D;
		z_radius = 0D;
	}

	@Override
	void setupUI() {
		//int id, String label, x, y, w, String initVal, int maxLen
		gui.addParamField(X_RAD, getParamNames().get(X_RAD), 10, 40, 50, Double.toString(x_radius), 5);
		gui.addParamField(Y_RAD, getParamNames().get(Y_RAD), 10, 120, 50, Double.toString(y_radius), 5);
		gui.addParamField(Z_RAD, getParamNames().get(Z_RAD), 10, 80, 50, Double.toString(z_radius), 5);
	}

	@Override
	void handleButtonInput(int buttonId) {
	}

	@Override
	void collectInput() {

		x_radius = NumberManip.stringToDouble_s(getParamFieldById(X_RAD).getText());
		
		y_radius = NumberManip.stringToDouble_s(getParamFieldById(Y_RAD).getText());
		
		z_radius = NumberManip.stringToDouble_s(getParamFieldById(Z_RAD).getText());
	}



	@Override
	void displayParams() {	//load the saved parameters back into the UI
		getParamFieldById(X_RAD).setText(Double.toString(x_radius));
		getParamFieldById(Y_RAD).setText(Double.toString(y_radius));
		getParamFieldById(Z_RAD).setText(Double.toString(z_radius));
	}

	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideEllipsoid guide = new GuideEllipsoid(origin, red, green, blue, alpha, this);
		guide.x_radius = this.x_radius;
		guide.y_radius = this.y_radius;
		guide.z_radius = this.z_radius;
		guide.initializeInstance();
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideEllipsoid) {
			GuideEllipsoid ellipGuide = (GuideEllipsoid) guide;
			this.x_radius = ellipGuide.x_radius;
			this.y_radius = ellipGuide.y_radius;
			this.z_radius = ellipGuide.z_radius;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
}
