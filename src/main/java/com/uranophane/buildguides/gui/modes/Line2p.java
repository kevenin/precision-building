package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideEllipsoid;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Line2p extends Mode{
	// constants for interfacing with the UI
	private static final String NAME = "2-Point Line";
	private static final int BUTTON_POINT1 	= 0;
	private static final int BUTTON_POINT2	= 1;
	private static final int MULT 			= 6;
	
	// interface parameters
	private double scalar = 1D;
	private Vec3i point1;
	private Vec3i point2;

	public Line2p(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public String getName() {
		return NAME;
	}

	private HashMap<Integer, String> getParamNames() {
		HashMap<Integer, String> paramNames = new HashMap();
		paramNames.put(0, "Point 1 x");
		paramNames.put(1, "Point 1 y");
		paramNames.put(2, "Point 1 z");
		paramNames.put(3, "Point 2 x");
		paramNames.put(4, "Point 2 y");
		paramNames.put(5, "Point 2 z");
		paramNames.put(MULT, "Length Multiplier");
		return paramNames;
	}

	@Override
	public void defParams() {
		scalar = 1d;
		point1 = new Vec3i(0,0,0);
		point2 = new Vec3i(0,0,0);
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(MULT, getParamNames().get(MULT), 10, 40, 50, Double.toString(scalar), 5);
		gui.addButton(BUTTON_POINT1, "Set Point 2", gui.modeButtonWidth + 10, 100, 200, 120);
		gui.addButton(BUTTON_POINT2, "Set Point 1", gui.modeButtonWidth + 10, 76, 200, 96);
	}

	@Override
	void handleButtonInput(int buttonId) {
		
		switch (buttonId) {
		case BUTTON_POINT1: 
			point1 = new Vec3i(GuidesMod.currentBlock.getX(), GuidesMod.currentBlock.getY(), GuidesMod.currentBlock.getZ());
			//System.out.println("Set point 1 to " + tempParams.get(POINT1_X) + ", " + tempParams.get(POINT1_Y) + ", " + tempParams.get(POINT1_Z));
			break;
		case BUTTON_POINT2:
			point2 = new Vec3i(GuidesMod.currentBlock.getX(), GuidesMod.currentBlock.getY(), GuidesMod.currentBlock.getZ());
			break;
		}
	}

	@Override
	void collectInput() {
		scalar = NumberManip.stringToDouble_s(getParamFieldById(MULT).getText());
	}

	@Override
	void displayParams() {
		getParamFieldById(MULT).setText(Double.toString(scalar));
	}

	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideLine2p guide = new GuideLine2p(origin, red, green, blue, alpha, this);
		guide.point1 = new Vec3d(point1);
		guide.point2 = new Vec3d(point2);
		guide.scalar = this.scalar;
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideLine2p) {
			GuideLine2p lineGuide = (GuideLine2p) guide;
			this.point1 = new Vec3i(lineGuide.point1.x, lineGuide.point1.y, lineGuide.point1.z);
			this.point2 = new Vec3i(lineGuide.point2.x, lineGuide.point2.y, lineGuide.point2.z);
			this.scalar = lineGuide.scalar;
	
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
	
}
