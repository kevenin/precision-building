package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.LabelledTextField;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

// modes are UI renderers and input handlers. Their ultimate goal is to produce guide objects
public abstract class Mode{
	public int index;
	public FontRenderer fr;
	protected SettingsGui gui;
	protected int counter = 0;

	public int incCounter() {
		this.counter++;
		return this.counter;
	}
	
	public int getCounter() {
		return this.counter;
	}
	
	public void resCounter() {
		this.counter = 0;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public Mode(SettingsGui parent) {
		this.gui = parent;
	}
	
	public static void appendVectorsNoRepeat(HashSet<Vec3i> pattern, ArrayList<Vec3i> appendee){	//array version
		for(Vec3i vec : appendee) {
			pattern.add(vec);
		}
	}
	
	public void setFR(FontRenderer fr) {
		this.fr = fr;
	}
	
	public LabelledTextField getParamFieldById(int id) {
		for (int i = 0; i < gui.modeFieldList.size(); i++) {
			if (gui.modeFieldList.get(i).getId() == id) {
				return gui.modeFieldList.get(i);
			}
		}
		return null;
	}
	
	//this function does not belong here
	public void renderLabels() {
		for (int i = 0; i < gui.modeFieldList.size(); i++) {
			gui.drawString(fr, gui.modeFieldList.get(i).name + ": ", gui.modeFieldList.get(i).x, gui.modeFieldList.get(i).y - fr.FONT_HEIGHT - 2, 16777215);
		}
	}
	
	public int getIndex() {
		return index;
	}
	
	public int countType() {
		int count = 0;
		for (Guide g : GuidesMod.guideList) {
			if (g.modeId == this.index) {
				count++;
			}
		} 
		return count;
	}
	
	public abstract String getName();
	public abstract void defParams();
	abstract void setupUI();
	void handleButtonInput(int buttonId) {}
	void collectInput() {}
	//public abstract ArrayList<Vec3d> generatePattern();
	void displayParams() {}
	
	void renderEvent() {}
	void renderUiEvent() {}
	
	// guide constructor. Needs input from main class. Returns deep copy
	public abstract Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha);
	public abstract void loadGuide(Guide guide);


}
