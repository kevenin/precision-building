package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.Collections;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideCircle;
import com.uranophane.buildguides.guides.GuideRays;
import com.uranophane.buildguides.util.NumberManip;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Rays extends Mode{
	private final int LENGTH = 0;	//id for length parameter
	private final int SPOKES = 1;	//id for points parameter (unused)
	
	private int spokes = 4;
	private double length = 0D;
	
	public Rays(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public
	String getName() {
		return "Star";
	}

	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add("Length");
		paramNames.add("Spokes");
		return paramNames;
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(LENGTH, getParamNames().get(LENGTH), 10, 40, 50, Double.toString(length), 5);
		gui.addParamField(SPOKES, getParamNames().get(SPOKES), 10, 80, 50, Integer.toString(spokes), 3);
	}

	@Override
	void collectInput() {
		length = NumberManip.stringToDouble_s(getParamFieldById(LENGTH).getText());
				
		spokes = (int) NumberManip.stringToDouble_s(getParamFieldById(SPOKES).getText());
	}


	@Override
	void handleButtonInput(int buttonId) {
	}


	@Override
	public
	void defParams() {
		length = 16D;
		spokes = 4;
	}

	@Override
	void displayParams() {
		getParamFieldById(LENGTH).setText(Double.toString(length));
		getParamFieldById(SPOKES).setText(Integer.toString(spokes));
	}


	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideRays guide = new GuideRays(origin, red, green, blue, alpha, this);
		guide.length = this.length;
		guide.spokes = this.spokes;
		guide.initializeInstance();
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideRays) {
			GuideRays raysGuide = (GuideRays) guide;
			this.length = raysGuide.length;
			this.spokes = raysGuide.spokes;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
}
