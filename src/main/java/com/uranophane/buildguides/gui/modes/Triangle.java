package com.uranophane.buildguides.gui.modes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideTriangle;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.util.RenderTools;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Triangle extends Mode{
	private static final String NAME = "3-Point Triangle";
	private static final int BUTTON_START 	= 0;
	private static final int BUTTON_CANCEL	= 1;
	private static final int POINT1 		= 0;
	private static final int POINT2			= 1;
	private static final int POINT3			= 2;
	private static final int MULT 			= 3;
	private static final float STEP_SIZE	= 0.5f;
	private double scalar = 1D;
	private Vec3d point1;
	private Vec3d point2;
	private Vec3d point3;
	
	private boolean drawing;
	private int numPoints;
	
	public Triangle(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public String getName() {
		return NAME;
	}


	@Override
	public void defParams() {
		scalar = 1d;
		drawing = false;
		numPoints = 0;
		point1 = new Vec3d(0,0,0);
		point2 = new Vec3d(0,0,0);
		point3 = new Vec3d(0,0,0);
	}
	
	private HashMap<Integer, String> getParamNames() {
		HashMap<Integer, String> paramNames = new HashMap();
		paramNames.put(POINT1, "Point 1");
		paramNames.put(POINT2, "Point 2");
		paramNames.put(POINT3, "Point 3");
		paramNames.put(MULT, "Size Multiplier");
		return paramNames;
	}
	
	@Override
	void setupUI() {
		gui.addParamField(MULT, getParamNames().get(MULT), 10, 40, 50, Double.toString(scalar), 5);
		gui.addButton(BUTTON_START, "Add Point (" + numPoints + "/3)", gui.modeButtonWidth + 10, 100, 200, 120);
		gui.addButton(BUTTON_CANCEL, "Cancel", gui.modeButtonWidth + 10, 76, 200, 96);
		if (!drawing)
			gui.disableButton(BUTTON_CANCEL);
	}
	@Override
	void handleButtonInput(int buttonId) {
		switch (buttonId) {
		case BUTTON_START: 
			drawing = true;
			
			// triangle drawing FSM
			switch(numPoints) {
			case 0:
				point1 = new Vec3d(GuidesMod.currentBlock);
				numPoints++;
				break;
			case 1:
				point2 = new Vec3d(GuidesMod.currentBlock);
				numPoints++;
				break;
			case 2:
				point3 = new Vec3d(GuidesMod.currentBlock);
				numPoints++;
				GuidesMod.registerGuide(GuidesMod.generateGuide(new BlockPos(point1)));
				break;
			case 3:
				point1 = new Vec3d(GuidesMod.currentBlock);
				numPoints = 1;
				break;
			default:
				point1 = new Vec3d(GuidesMod.currentBlock);
				numPoints = 1;
			}
			break;
		case BUTTON_CANCEL:
			drawing = false;
			numPoints = 0;
			
			break;
		}
		gui.updateUI();
	}

	@Override
	void collectInput() {
		scalar = NumberManip.stringToDouble_s(getParamFieldById(MULT).getText());
	}

	public static ArrayList<Vec3d> generateTri(Vec3d p1, Vec3d p2, Vec3d p3, double s){
		Set<Vec3d> pattern = new HashSet();
		double u, v = 0;
		Vec3d side1 = p3.subtract(p1);
		Vec3d side2 = p2.subtract(p1);
		double len1 = p3.distanceTo(p1);
		double len2 = p2.distanceTo(p1);
		double du = STEP_SIZE / len1;
		double dv = STEP_SIZE / len2;
		for (u = 0 - (s - 1); u <= s + (s - 1); u += du) {
			for (v = 0 - (s - 1); v <= s - u; v += dv) {
				pattern.add(side1.scale(u).add(side2.scale(v)));
			}
		}
		return new ArrayList<Vec3d>(pattern);
	}
	
	@Override
	public Guide getGuide(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideTriangle guide = new GuideTriangle(new BlockPos(point1), red, green, blue, alpha, this);
		guide.point1 = new Vec3d(point1.x, point1.y, point1.z);
		guide.point2 = new Vec3d(point2.x, point2.y, point2.z);
		guide.point3 = new Vec3d(point3.x, point3.y, point3.z);
		guide.scalar = this.scalar;
		guide.initializeInstance();
		return guide;
	}

	@Override
	public void loadGuide(Guide guide) {
		if (guide instanceof GuideTriangle) {
			numPoints = 3;
			GuideTriangle triGuide = (GuideTriangle) guide;
			this.point1 = new Vec3d(triGuide.point1.x, triGuide.point1.y, triGuide.point1.z);
			this.point2 = new Vec3d(triGuide.point2.x, triGuide.point2.y, triGuide.point2.z);
			this.point3 = new Vec3d(triGuide.point3.x, triGuide.point3.y, triGuide.point3.z);
			this.scalar = triGuide.scalar;
		}
	}
	
	@Override
	public void renderEvent() {
		if (numPoints > 1)
			RenderTools.drawLineOnWorld(point1.addVector(0.5d, 0.5d, 0.5d), point2.addVector(0.5d, 0.5d, 0.5d), 1f, 1f, 0f, 1f);
		if (numPoints > 2) {
			RenderTools.drawLineOnWorld(point2.addVector(0.5d, 0.5d, 0.5d), point3.addVector(0.5d, 0.5d, 0.5d), 1f, 1f, 0f, 1f);
			RenderTools.drawLineOnWorld(point3.addVector(0.5d, 0.5d, 0.5d), point1.addVector(0.5d, 0.5d, 0.5d), 1f, 1f, 0f, 1f);
		}
			
	}

}
