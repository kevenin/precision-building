package com.uranophane.buildguides.gui.textfields;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiTextField;

public class LabelledTextField extends GuiTextField{
	public String name;
	private final FontRenderer fr;
	/**
	 * Generates a string labelled text field object 
	 * @param id a unique ID for the field
	 * @param label the text to display above the field
	 * @param x starting x
	 * @param y starting y
	 * @param w width of the field
	 * @param initVal default string
	 * @param maxLen maximum string length
	 */
	public LabelledTextField(FontRenderer fontrendererObj, int id, String label, int x, int y, int w, String initVal, int maxLen) {
		super(id, fontrendererObj, x, y, w, 20);
		this.fr = fontrendererObj;
		this.setMaxStringLength(maxLen);
		this.setText(initVal);
	
		//set the title
		this.setName(label);
	}
	
	/**
	 * Draws the text field
	 * @param parent parent GUI
	 */
	public void drawWithLabel(Gui parent) {
		this.drawTextBox();
		parent.drawString(fr , this.name, this.x, this.y - fr.FONT_HEIGHT - 2, 16777215);
	}

	public void setName(String label) {
		this.name = label;
	}
	
	public boolean textboxKeyTyped(char typedChar, int keyCode){
		return super.textboxKeyTyped(typedChar, keyCode);
    }
}
