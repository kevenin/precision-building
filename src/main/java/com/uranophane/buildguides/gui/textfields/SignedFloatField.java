package com.uranophane.buildguides.gui.textfields;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.ChatAllowedCharacters;

public class SignedFloatField extends LabelledTextField{
	private final char[] float_chars = {'0','1','2','3','4','5','6','7','8','9','0','.','-'};
	private final int[] edit_keys = {14, 211, 203, 205};
	
	public SignedFloatField(FontRenderer fontrendererObj, int id, String label, int x, int y, int w, String initVal,
			int maxLen) {
		super(fontrendererObj, id, label, x, y, w, initVal, maxLen);

	}

	private boolean isFloatChar(char c) {
		for (int i = 0; i < float_chars.length; i++) {
			if (c == float_chars[i])
				return true;
		}
		return false;
	}
	
	private boolean isValidKey(int id) {
		for (int i = 0; i < edit_keys.length; i++) {
			if (id == edit_keys[i])
				return true;
		}
		return false;
	}
		

	@Override
	public boolean textboxKeyTyped(char typedChar, int keyCode)
    {
		if (isFloatChar(typedChar) || isValidKey(keyCode)) {	//only register if the key is a number, decimal or backspace

			return super.textboxKeyTyped(typedChar, keyCode);
		}
		return false;
    }

}
