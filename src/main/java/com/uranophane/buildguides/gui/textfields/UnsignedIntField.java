package com.uranophane.buildguides.gui.textfields;

import net.minecraft.client.gui.FontRenderer;

public class UnsignedIntField extends LabelledTextField{
	
	private final char[] int_chars = {'0','1','2','3','4','5','6','7','8','9'};
	private final int[] edit_keys = {14, 211, 203, 205};
	
	public UnsignedIntField(FontRenderer fontrendererObj, int id, String label, int x, int y, int w, String initVal,
		int maxLen) {
		super(fontrendererObj, id, label, x, y, w, initVal, maxLen);
	}
	
	private boolean isIntChar(char c) {
		for (int i = 0; i < int_chars.length; i++) {
			if (c == int_chars[i])
				return true;
		}
		return false;
	}
	
	private boolean isValidKey(int id) {
		for (int i = 0; i < edit_keys.length; i++) {
			if (id == edit_keys[i])
				return true;
		}
		return false;
	}
	
	@Override
	public boolean textboxKeyTyped(char typedChar, int keyCode)
    {
		if (isIntChar(typedChar) || isValidKey(keyCode)) {	//only register if the key is a number, decimal or backspace

			return super.textboxKeyTyped(typedChar, keyCode);
		}
		return false;
    }
	
}
