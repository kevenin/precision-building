package com.uranophane.buildguides.guides;

import com.uranophane.buildguides.GuidesMod;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class BezierNode{
	public Vec3d origin;
	public Vec3d center;
	public Vec3d prehandle;
	public Vec3d posthandle;
	public double prelength;
	public double postlength;
	private boolean isSelected;
	public BezierNode(Vec3d center, Vec3d post) {
		this.origin = center;
		this.center = origin.addVector(0.5, 0.5, 0.5);
		this.posthandle = new Vec3d(Math.round(post.x), Math.round(post.y), Math.round(post.z)).addVector(0.5,0.5,0.5);
		this.prehandle = this.origin.subtract(post);
	}
	
	public BezierNode(BezierNode node) {
		this.origin = node.origin;
		this.center = node.center;
		this.posthandle = node.posthandle;
		this.prehandle = node.prehandle;
	}
	
	public void drawNode() {
		BlockPos centerBlock = new BlockPos(center);
		BlockPos preBlock = new BlockPos(prehandle);
		BlockPos postBlock = new BlockPos(posthandle);
		if (!isSelected) {
			GuidesMod.drawBoxOnWorld(GuidesMod.player, centerBlock, GuidesMod.partialTicks, 0, 0, 1, 1);
			GuidesMod.drawLineOnWorld(GuidesMod.player, prehandle, posthandle, GuidesMod.partialTicks, 0, 0, 1, 1);
		}
		else {
			GuidesMod.drawBoxOnWorld(GuidesMod.player, centerBlock, GuidesMod.partialTicks, 1, 0, 1, 1);
			GuidesMod.drawLineOnWorld(GuidesMod.player, prehandle, posthandle, GuidesMod.partialTicks, 1, 0, 1, 1);
		}
		GuidesMod.drawBoxOnWorld(GuidesMod.player, preBlock, GuidesMod.partialTicks, 1, 0, 0, 1);
		GuidesMod.drawBoxOnWorld(GuidesMod.player, postBlock, GuidesMod.partialTicks, 0, 1, 0, 1);
		
	}

	public void setPosthandle(Vec3d location) {
		this.posthandle = new Vec3d(Math.round(location.x), Math.round(location.y), Math.round(location.z)).addVector(0.5,0.5,0.5);
	}
	
	public void setOrigin(Vec3d location) {
		this.origin = location;
		this.center = origin.addVector(0.5, 0.5, 0.5);
	}
	
	public void setPrehandle(Vec3d location) {
		this.prehandle = location;
	}

	public void rectify() {
		this.prehandle = this.center.subtract(posthandle).add(center);
	}

	public void select() {
		this.isSelected = true;
	}
	
	public void deselect() {
		this.isSelected = false;
	}

	public void reposition(BlockPos currentBlock) {
		Vec3d delta = new Vec3d(currentBlock.getX() - origin.x, currentBlock.getY() - origin.y, currentBlock.getZ() - origin.z);
		this.origin = new Vec3d(currentBlock);
		this.center = origin.addVector(0.5, 0.5, 0.5);
		this.prehandle = prehandle.add(delta);
		this.posthandle = posthandle.add(delta);
	}

}
