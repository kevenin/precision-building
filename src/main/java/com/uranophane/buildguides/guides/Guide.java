package com.uranophane.buildguides.guides;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.lwjgl.opengl.GL11;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.GuidesMod.FadeMode;
import com.uranophane.buildguides.cmd.CmdHelp;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.util.RenderTools;
import com.uranophane.buildguides.util.TransformGuide;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

//a pattern has a color, pattern (set of 3d vectors) and uses partialticks and player information from the drawMouseHighlight event
//the origin controls the whole pattern
public abstract class Guide implements Serializable{
	
	private static final double TOLERANCE = 0.5;
	private static final float XRAY_ALPHA = 0.4f;
	private static final double ROT_CROSS_LEN = 10;
	public BlockPos origin = new BlockPos(0,0,0);
	protected transient ArrayList<Vec3d> unmodifiedCopy; //for rotations and other transformations
	protected BlockPos oldOrigin = new BlockPos(0,0,0);
	public transient ArrayList<Vec3d> rawCoords = new ArrayList();	//transformed version of the above
	public transient HashSet<Vec3i> patternCoords = new HashSet(); 	//rasterized poslist
	private transient VertexBuffer airVBO = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
	private transient VertexBuffer xrayVBO = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
	private transient VertexBuffer cheapRender = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
	private TransformGuide tf = new TransformGuide();
	protected transient Mode modeObj;
	
	public boolean visible;
	public boolean drawCheap;
	public boolean xray;
	public boolean selected;
	public int modeId;
	public float red, green, blue, alpha;
	protected String name;
	
	/**
	 * Moves the origin
	 * @param origin
	 */
	public void setOrigin(BlockPos origin) {
		if (this.origin != origin) {
			this.origin = origin.toImmutable();
		}
	}
	
	public void setName(String name) {
		this.name = new String(name);
	}
	
	public String getName() {
		return new String(this.name);
	}
	
	public Guide(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		this.origin = origin;
		this.oldOrigin = origin;
		this.name = modeObj.getName() + Integer.toString(modeObj.incCounter());
		this.modeObj = modeObj;
		this.modeId = modeObj.index;
		this.visible = true;
		this.drawCheap = true;
		this.xray 	= true;
		this.red 	= red;
		this.green 	= green;
		this.blue 	= blue;
		this.alpha 	= alpha;
		this.initializeInstance();
	}
	
	public Guide() {
	}
	
	public boolean isInitialized() {
		return !(this.modeObj == null || this.unmodifiedCopy == null || this.airVBO == null || this.xrayVBO == null);
	}
	
	public void initializeInstance() {
		this.modeObj = GuidesMod.getMode(this.modeId);
		this.unmodifiedCopy = this.generatePattern();					//non-rasterized, original
		this.rawCoords = new ArrayList<Vec3d>(this.unmodifiedCopy);			//non-rasterized, transformed
		this.patternCoords 	= rasterize(rawCoords);							//rasterized, transformed
		this.airVBO = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
		this.xrayVBO = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
		this.cheapRender = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
		generateVBO(airVBO, 1F, false);		// the overground vbo
		generateVBO(xrayVBO, XRAY_ALPHA, false);	// the xray vbo
		updateCheapVBO();
	}
	
	public boolean hasBlockPos(BlockPos block) {
		return patternCoords.contains(block.subtract(origin));
	}
	
	public HashSet<Vec3i> rasterize(ArrayList<Vec3d> patternd){
		
		HashSet<Vec3i> patterni = new HashSet();
		patterni.add(this.origin);
		for (int i = 0; i < patternd.size(); i++) {
			long x = Math.round(patternd.get(i).x);
			long y = Math.round(patternd.get(i).y);
			long z = Math.round(patternd.get(i).z);
			
			Vec3i roundedVec = new Vec3i(x,y,z);
			if(roundedVec.distanceSq(patternd.get(i).x, patternd.get(i).y, patternd.get(i).z) <= TOLERANCE)
				patterni.add(roundedVec);
		}
		
		return patterni;
	}
	
	public RayTraceResult detectLook(EntityPlayer player, double reach) {
		Vec3d eyepos = player.getPositionVector().addVector(0, 1, 0);
		return rayTracePattern(eyepos, player.getLookVec().scale(reach).add(eyepos));
	}
	
	public RayTraceResult rayTracePattern(Vec3d start, Vec3d end) {
		for (Vec3i block : patternCoords) {
			AxisAlignedBB guideBlock = new AxisAlignedBB((BlockPos) block).expand(0.01, 0.01, 0.01);
			RayTraceResult testTrace = GuidesMod.rayTrace((BlockPos) block, start, end, guideBlock);
			if (testTrace != null) {
				return testTrace;
			}
		}
		AxisAlignedBB guideBlock = new AxisAlignedBB((BlockPos) origin).expand(0.1, 0.1, 0.1);
		RayTraceResult testTrace = GuidesMod.rayTrace((BlockPos) origin, start, end, guideBlock);
		return testTrace;
	}
	
	public void setVis(boolean visibility) {
		this.visible = visibility;
	}
	/**
	 * Generates the VBO containing the fancy, dynamic block highlights for the entire Guide.
	 * Time dependant.
	 * @param vbo the vbo to modify
	 * @param alphaMult transparency modifier
	 * @param filled if false, the blocks will be wireframes; if true, the blocks will be shaded
	 * @return The generated VBO
	 */
	protected VertexBuffer generateVBO(VertexBuffer vbo, float alphaMult, boolean filled) {
		float alpha = MathHelper.clamp(alphaMult * this.alpha, 0f, 1f);
		BufferBuilder buffer = Tessellator.getInstance().getBuffer();
		
		if (vbo == null) {
			vbo = new VertexBuffer(DefaultVertexFormats.POSITION_COLOR);
		}
		
		if (filled) {
			buffer.begin(GL11.GL_TRIANGLE_STRIP, DefaultVertexFormats.POSITION_COLOR);
		}
		else
			buffer.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION_COLOR);
		
		/* iterate through coords array and draw each block */
		for (Vec3i block : patternCoords) {
		//initialize position at origin
			BlockPos blockPos = offsetBlockPos(origin, block.getX(), block.getY(), block.getZ());
		//offset from origin
			RenderTools.pushBlockToVBO(buffer, blockPos, red, green, blue, alpha, filled, FadeMode.ALPHA_SIZE_FADE);
		} 
		
		buffer.finishDrawing();
	//copy the VBO out
		vbo.bufferData(buffer.getByteBuffer());
		return vbo;
	}
	
	/**
	 * Cheap block model updater for THIS Guide. Runs asynchronously with rendering
	 */
	public void updateCheapVBO() {
		BufferBuilder buffer = Tessellator.getInstance().getBuffer();
		buffer.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION_COLOR);
		
		for (Vec3i block : patternCoords) {
			BlockPos blockPos = origin;
			blockPos = offsetBlockPos(blockPos, block.getX(), block.getY(), block.getZ());
			RenderTools.pushCheapBlockToVBO(buffer, blockPos, red, green, blue);
			} 
			buffer.finishDrawing();
	//copy the VBO out
		cheapRender.bufferData(buffer.getByteBuffer());
	}
	
	/**
	 * Dynamic block model updater for THIS Guide. Runs synchronously with rendering
	 */
	public void updateDynamicVBO() {
		// generate the dynamic vbos
		if (visible) {
			try {
				if (this.selected)
					generateVBO(airVBO, 0.75F, true);
				else
					generateVBO(airVBO, 1F, false);
				
				if (xray) {
					generateVBO(xrayVBO, XRAY_ALPHA, false);
					// TODO: add super-xray mode
				}
			}
			catch (Exception renderError) {}
		}
	}
	
	public void renderPattern() {

		try {
			if (this.visible) {
			// update the dynamic part	
				RenderTools.usingWireframeGlState(() -> {
					
					GlStateManager.pushMatrix();
				// offset to align with world
					GlStateManager.translate(-RenderTools.rx, -RenderTools.ry, -RenderTools.rz);
					
					RenderTools.usingBuffer(airVBO, () -> {
						GlStateManager.glLineWidth(3F);
						if (this.selected)
							airVBO.drawArrays(GL11.GL_TRIANGLE_STRIP);
						else
							airVBO.drawArrays(GL11.GL_LINE_STRIP);
					});
		
				
				// if xray, draw the underground vbo
					if (this.xray) {
						RenderTools.usingBuffer(xrayVBO, () -> {
							GlStateManager.glLineWidth(2F);
							GlStateManager.depthFunc(GL11.GL_GREATER);
							xrayVBO.drawArrays(GL11.GL_LINE_STRIP);
						});
					}
					
				// draw the cheap version
					if (drawCheap) {
						GlStateManager.depthFunc(GL11.GL_LEQUAL);
						RenderTools.usingBuffer(cheapRender, () -> {
							GlStateManager.glLineWidth(2F);
							cheapRender.drawArrays(GL11.GL_LINE_STRIP);
						});
					}
		
					GlStateManager.popMatrix();
				
				// draw the origin
					RenderTools.setTempRenderDist(100);
					GlStateManager.depthFunc(GL11.GL_ALWAYS);
					RenderTools.drawCubeOnWorld(origin, 1F - red, 1F - green, 1F - blue, 0.5F);
					RenderTools.drawBoxOnWorld(origin, 1F - red, 1F - green, 1F - blue, 1F);
					RenderTools.revertRenderDist();
					GlStateManager.depthFunc(GL11.GL_LEQUAL);
					
				// draw the rotation origin
					if (this.selected) {
						RenderTools.setTempRenderDist(100);
						GlStateManager.depthFunc(GL11.GL_ALWAYS);
						RenderTools.drawLineOnWorld(tf.origin.add(new Vec3d(origin)).addVector(ROT_CROSS_LEN + 0.5, 0.5, 0.5), 
													tf.origin.add(new Vec3d(origin)).addVector(-ROT_CROSS_LEN + 0.5, 0.5, 0.5), 1, 1, 1, 1);
						RenderTools.drawLineOnWorld(tf.origin.add(new Vec3d(origin)).addVector(0.5, ROT_CROSS_LEN + 0.5, 0.5), 
													tf.origin.add(new Vec3d(origin)).addVector(0.5, -ROT_CROSS_LEN + 0.5, 0.5), 1, 1, 1, 1);
						RenderTools.drawLineOnWorld(tf.origin.add(new Vec3d(origin)).addVector(0.5, 0.5, ROT_CROSS_LEN + 0.5), 
													tf.origin.add(new Vec3d(origin)).addVector(0.5, 0.5, -ROT_CROSS_LEN + 0.5), 1, 1, 1, 1);
						RenderTools.revertRenderDist();
						GlStateManager.depthFunc(GL11.GL_LEQUAL);
					}
				});
				
			}
		}
		catch (NullPointerException npe) {
			System.err.println("[Guides] Guide VBO not initialized.");
		}
	}
	
	//offsets a BlockPos.
	public static BlockPos offsetBlockPos(BlockPos blockpos, int x, int y, int z) {
		blockpos = blockpos.south(z).east(x).up(y);
		return blockpos;
	}
	
	protected abstract ArrayList<Vec3d> generatePattern();
	public abstract ArrayList<Float> getParamArray();
	public abstract String getDisplayString();

	public void toggleVis() {
		this.visible = visible ? false : true;
	}
	
	public void toggleXray() {
		this.xray = xray ? false : true;
	}
	
	public void toggleCheap() {
		this.drawCheap = drawCheap ? false : true;
	}
	
	/**
	 * Transform using interal memory
	 */
	public void doTransform() {
		//CmdHelp.printToChat("Matrix:\n" + tf.toString());
		origin = oldOrigin;
		Vec3d newOrigin = tf.transform(new Vec3d(0,0,0));
		origin = origin.add(newOrigin.x, newOrigin.y, newOrigin.z);
		rawCoords.clear();
		for (Vec3d point : unmodifiedCopy) {
			rawCoords.add(tf.transform(point).subtract(newOrigin));
		}
		this.patternCoords = rasterize(rawCoords);
		updateCheapVBO();
	}
	
	public void mergeTransform() {
		tf.calculateTf();
		tf.clear();
		tf.addTfMatx(tf.getMatrix());
	}

	public TransformGuide getTransform() {
		return this.tf;
	}
}