package com.uranophane.buildguides.guides;

import java.util.ArrayList;
import java.util.HashSet;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import scala.Int;

public class GuideArb extends Guide{
	public Vec3i point1;
	public Vec3i point2;

	public GuideArb(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		return null;
	}

	@Override
	public String getDisplayString() {
		return "Blocks: " + this.patternCoords.size();
	}

	@Override
	protected ArrayList<Vec3d> generatePattern() {
		World world = GuidesMod.player.getEntityWorld();
		HashSet<Vec3i> patternI = new HashSet();
		
		for (int i = point1.getX(); i <= point2.getX(); i++) {
			for (int j = point1.getY(); j <= point2.getY(); j++) {
				for (int k = point1.getZ(); k <= point2.getZ(); k++) {
					
					if (!world.isAirBlock(new BlockPos(i, j, k))) {
						patternI.add(new Vec3i(i - origin.getX(), j - origin.getY(), k - origin.getZ()));
					}
				}
			}
		}
		patternI = VectorManip.getHollowVec3iSet(patternI);
		ArrayList<Vec3d> patternD = new ArrayList();
		VectorManip.batchVec3iToVec3d(patternI, patternD);
		return patternD;
	}
	
	
}
