package com.uranophane.buildguides.guides;

import java.util.ArrayList;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.util.TransformGuide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideArray extends Guide{

	public ArrayList<Vec3d> duplicand;
	public TransformGuide propagate;
	public int reps;
	
	public GuideArray(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		return null;
	}

	@Override
	public String getDisplayString() {
		return reps + " Reptitions";
	}
	
	@Override
	public ArrayList<Vec3d> generatePattern() {
		try {
			ArrayList<Vec3d> sum = new ArrayList();
			ArrayList<Vec3d> stamp = new ArrayList(duplicand);
			sum.addAll(stamp);
			//GuidesMod.printToChat("Propagation matrix: \n" + propagate.toString());
			for (int i = 0; i < reps; i++) {
				stamp = TransformGuide.xformVec3dList(stamp, this.propagate);
				sum.addAll(stamp);
			}
			return sum;
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return new ArrayList<Vec3d>();
		}
	}
}
