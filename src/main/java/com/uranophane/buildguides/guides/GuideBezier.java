package com.uranophane.buildguides.guides;

import java.util.ArrayList;
import java.util.LinkedList;

import org.lwjgl.opengl.GL11;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.modes.Bezier;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.util.BezierNode;
import com.uranophane.buildguides.util.RenderTools;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideBezier extends Guide{
	private static final double STEP_SIZE = 0.2D;
	
	public boolean isLoop;
	public LinkedList<BezierNode> nodes;
	public ArrayList<ArrayList<Vec3d>> arcs = new ArrayList();

	public GuideBezier(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {

		return null;
	}

	@Override
	public String getDisplayString() {
		return "Nodes: " + this.nodes.size();
	}
	
	private void calculateArcs() {
		arcs.clear();
		for(int i = 0; i < nodes.size() - 1; i++) {
			arcs.add(i, Bezier.bezierArc(nodes.get(i), nodes.get(i + 1)));
		}
		if (isLoop == true) {
			arcs.add(nodes.size() - 1, Bezier.bezierArc(nodes.getLast(), nodes.getFirst()));
		}
	}
	
	@Override
	public ArrayList<Vec3d> generatePattern() {
		ArrayList<Vec3d> pattern = new ArrayList();
		calculateArcs();
		for (ArrayList<Vec3d> arc : arcs) {
			for(int i = 0; i < arc.size(); i++) {
				pattern.add(arc.get(i).subtract(0.5,0.5,0.5));
			}
		}
		return pattern;
	}
}

