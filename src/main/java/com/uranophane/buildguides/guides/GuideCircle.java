package com.uranophane.buildguides.guides;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.gui.modes.Circle;
import com.uranophane.buildguides.gui.modes.Mode;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideCircle extends Guide{
	private final double STEP_SIZE = 1/3f;	//step size for circle generation
	
	public double radius = 1D;

	public GuideCircle(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		ArrayList<Float> params = new ArrayList();
		params.add((float) radius);
		return params;
	}

	@Override
	public String getDisplayString() {
		return "Radius: " + Double.toString(radius);
	}

	@Override
	public ArrayList<Vec3d> generatePattern() {
		Set<Vec3d> pattern = new HashSet();
		double radius = this.radius;
		
		// uses the sqrt(r^2-x^2) method. x starts at 0, y starts at the maximum, r.
		double x = 0;
		double y = radius;
		
		//draws octant from 0 until y<=x.
		do {
			if (radius <= 1)
				break;
			//add (x,y) to the vector list as (x, 0, y)
			pattern.add(new Vec3d(x, 0, y)); //repeat for all 8 octants
			pattern.add(new Vec3d(-x, 0, y)); 
			pattern.add(new Vec3d(x, 0, -y)); 
			pattern.add(new Vec3d(-x, 0, -y)); 
			pattern.add(new Vec3d(y, 0, x)); 
			pattern.add(new Vec3d(-y, 0, x)); 
			pattern.add(new Vec3d(y, 0, -x)); 
			pattern.add(new Vec3d(-y, 0, -x)); 
			x += STEP_SIZE;
			y = Math.sqrt(Math.pow(radius, 2D) - Math.pow(x, 2D)); //y = sqrt( r^2 - x^2 )
		}
		while(!(y < x));
		
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}

}
