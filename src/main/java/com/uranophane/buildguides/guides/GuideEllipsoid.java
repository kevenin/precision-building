package com.uranophane.buildguides.guides;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.gui.modes.Ellipsoid;
import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideEllipsoid extends Guide{
	private static final double STEP_SIZE = 0.5d;	//step size for sampling
	
	public double x_radius = 0D;
	public double y_radius = 0D;
	public double z_radius = 0D;

	public GuideEllipsoid(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		ArrayList<Float> params = new ArrayList();
		params.add((float) x_radius);
		params.add((float) y_radius);
		params.add((float) z_radius);
		return params;
	}

	@Override
	public String getDisplayString() {
		return "X: " + Double.toString(x_radius) + " Y: " + Double.toString(y_radius) + " Z: " + Double.toString(z_radius);
	}
	
	@Override
	public ArrayList<Vec3d> generatePattern() {

		//use set to prevent duplicates
		Set<Vec3d> pattern = new HashSet();
		double A = x_radius;
		double B = y_radius;
		double C = z_radius;

		
		//sample the same relation 3 times from 3 directions: x^2 + y^2 + z^2 = 1
		for (int u = 0; u < 3; u++) {
			double a = 0D;
			double b = 0D;
			double c = 0D;
			switch (u) {
			case 0:	//evaluating for x, y: c*sqrt(1-(x/a)^2-(y/b)^2)
				a = A;
				b = B;
				c = C;
				break;
			case 1:	//evaluating for y, z: a*sqrt(1-(y/b)^2-(z/c)^2)
				a = B;
				b = C;
				c = A;
				break;
			case 2:	//evaluating for x, z: b*sqrt(1-(x/a)^2-(z/c)^2)
				a = A;
				b = C;
				c = B;
				break;
			}
			for (double i = 0; i <= Math.round(a); i += STEP_SIZE) {
				for (double j = 0; j <= Math.round(b); j += STEP_SIZE) {
					double k = 0;
					if (Double.isNaN(Math.sqrt(1 - Math.pow(i/a, 2F) - Math.pow(j/b, 2F)))){
						continue;
					} 
					else {
						k = c * Math.sqrt(1 - Math.pow(i/a, 2F) - Math.pow(j/b, 2F));
						if (k < c/2) {
							break;
						}
					}
					double x = 0;
					double y = 0;
					double z = 0;
					switch (u) {
					case 0:	//evaluating for x, y: c*sqrt(1-(x/a)^2-(y/b)^2)
						x = i;
						y = j;
						z = k;
						break;
					case 1:	//evaluating for y, z: a*sqrt(1-(y/b)^2-(z/c)^2)
						x = k;
						y = i;
						z = j;
						break;
					case 2:	//evaluating for x, z: b*sqrt(1-(x/a)^2-(z/c)^2)
						x = i;
						y = k;
						z = j;
						break;
					}
					VectorManip.appendVec3dOctuple(pattern, new Vec3d(x, y, z));
				}
			}
		}
		//formula: X: a*sqrt(1-(y/b)^2-(z/c)^2)
		
		//convert set to list
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}
}
