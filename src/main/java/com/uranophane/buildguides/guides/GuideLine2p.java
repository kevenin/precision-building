package com.uranophane.buildguides.guides;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.gui.modes.Line2p;
import com.uranophane.buildguides.gui.modes.Mode;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideLine2p extends Guide{
	private static final double STEP_SIZE 	= 1/3d;
	
	public double scalar = 6;
	public Vec3d point1 = new Vec3d(0,0,0);
	public Vec3d point2 = new Vec3d(0,0,0);
	
	public GuideLine2p(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		ArrayList<Float> params = new ArrayList();
		params.add((float) point1.x);
		params.add((float) point1.y);
		params.add((float) point1.z);
		params.add((float) point2.x);
		params.add((float) point2.y);
		params.add((float) point2.z);
		params.add((float) scalar);
		return params;
	}

	@Override
	public String getDisplayString() {
		double length = this.scalar * this.point1.distanceTo(point2);
		// round to the third decimal point
		length = length - length % 0.001D;
		return "Length: " + Double.toString(length);
	}
	
	@Override
	public ArrayList<Vec3d> generatePattern() {
		Set<Vec3d> pattern = new HashSet();
		double t = 0F;
		final Vec3d startAbs= point1;
		final Vec3d endAbs 	= point2;
		
		final Vec3d start	= new Vec3d(0, 0, 0);
		final Vec3d end		= endAbs.subtract(startAbs);
		final Vec3d middle	= end.scale(1/2d);
		final Vec3d dirn 	= end.normalize();
		double max_t = start.distanceTo(end) * scalar;
		
		for (t = 0F; t <= max_t / 2; t += STEP_SIZE) {
			pattern.add(dirn.scale(t).add(middle));
			pattern.add(dirn.scale(-t).add(middle));
		}
		
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}
}
