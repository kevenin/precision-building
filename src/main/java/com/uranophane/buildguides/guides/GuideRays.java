package com.uranophane.buildguides.guides;

import java.util.ArrayList;

import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.gui.modes.Rays;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideRays extends Guide{
	private static final double STEP_SIZE = 1/2d;
	public double length = 0D;
	public int spokes = 4;
	
	public GuideRays(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		ArrayList<Float> params = new ArrayList();
		params.add((float) length);
		params.add((float) spokes);
		return params;
	}

	@Override
	public String getDisplayString() {
		return "Points: " + Integer.toString(spokes);
	}
	
	@Override
	public ArrayList<Vec3d> generatePattern() {
		ArrayList<Vec3d> pattern = new ArrayList();
		long size 		= Math.round(length);
		int symmetry 	= spokes;
		double full_circle = 2 * Math.PI;
		double angle	= 0d;
		
		//grow size times in each symmetry direction

		for (double i = 0; i <= size; i += STEP_SIZE) {			//first loop is radius
			for (int j = 0; j < symmetry; j++) {				//second loop is n-sector
				angle = j * full_circle / symmetry;
				pattern.add(new Vec3d(Math.cos(angle), 0, Math.sin(angle)).scale(i));
			}
		}
		return pattern;
	}
}
