package com.uranophane.buildguides.guides;

import java.util.ArrayList;

import com.uranophane.buildguides.gui.modes.Mode;
import com.uranophane.buildguides.gui.modes.Triangle;
import com.uranophane.buildguides.util.NumberManip;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuideTriangle extends Guide{
	private static final float STEP_SIZE	= 1/3f;
	
	public double scalar = 1D;
	public Vec3d point1;
	public Vec3d point2;
	public Vec3d point3;
	
	public GuideTriangle(BlockPos origin, float red, float green, float blue, float alpha, Mode modeObj) {
		super(origin, red, green, blue, alpha, modeObj);
	}

	@Override
	public ArrayList<Float> getParamArray() {
		return null;
	}

	@Override
	public String getDisplayString() {
		Vec3d side1 = point3.subtract(point1);
		Vec3d side2 = point2.subtract(point1);
		double area = side1.crossProduct(side2).lengthSquared();
		area = Math.sqrt(area)/2;
		
		return "Area: " + Double.toString(area).substring(0, 6);
	}

	@Override
	public ArrayList<Vec3d> generatePattern() {
		return Triangle.generateTri(point1, point2, point3, scalar);
	}
}
