package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.LinkedList;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.BezierNode;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideBezier;
import com.uranophane.buildguides.util.VectorManip;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class Bezier extends Mode{

	private static final int NULL_SELECT = 404000;
	
	// shared with the guide
	private LinkedList<BezierNode> nodes = new LinkedList();
	private boolean isLoop;
	
	// ui exclusive
	private int currentIndex;
	private boolean editing;
	private boolean moving;
	private BezierNode old_node;
	public boolean showNodes;
	private boolean reverse;
	private ArrayList<ArrayList<Vec3d>> arcs = new ArrayList();
	
	private static final double STEP_SIZE = 0.2D;
	
	private static final int BUTTON_SEL = 0;
	private static final int BUTTON_INSPRE = 1;
	private static final int BUTTON_INSPOST = 2;
	private static final int BUTTON_DEL = 3;
	private static final int BUTTON_EDIT = 4;
	private static final int BUTTON_CONF = 5;
	private static final int BUTTON_MOV = 6;
	private static final int BUTTON_NEW = 7;
	private static final int BUTTON_CLOSE = 8;
	private static final int BUTTON_SHOW = 9;
	private static final int BUTTON_SELNEXT = 10;
	private static final int BUTTON_SELPREV = 11;
	private static final int BUTTON_CANCEL = 12;
	private static final int BUTTON_FLIP = 13;

	public Bezier(SettingsGui parent) {
		super(parent);
		defParams();
	}
	
	 private static Vec3d line2P(Vec3d start, Vec3d end, double t){
	        Vec3d newPos = new Vec3d(0,0,0);

	        newPos = newPos.add(start);
	        end = end.subtract(start);
	        end = end.scale(t);
	        newPos = newPos.add(end);

	        return newPos;
	 }

    protected static Vec3d bezierPoint(ArrayList<Vec3d> points, double t){
        if (t < 0 || t > 1) t = t % 1;
        ArrayList<Vec3d> reducedPoints = new ArrayList<>();
        if (points.size() == 1) return points.get(0);
        for (int i = 1; i < points.size(); i++) {
            Vec3d p1 = points.get(i - 1);
            Vec3d p2 = points.get(i);
            reducedPoints.add(line2P(p1, p2, t));
        }
        return bezierPoint(reducedPoints, t);
    }

    private ArrayList<Vec3d> bezierArc(BezierNode node1, BezierNode node2){
    	double t = 0;
    	double dt = STEP_SIZE/VectorManip.getVec3dDist(node1.center, node2.center);
    	Vec3d lastPoint = new Vec3d(node1.center.x, node1.center.y, node1.center.z);
    	Vec3d newPoint = null;
    	ArrayList<Vec3d> arc = new ArrayList();
    	ArrayList<Vec3d> controls = new ArrayList();
    	controls.add(node1.center);
    	controls.add(node1.posthandle);
    	controls.add(node2.prehandle);
    	controls.add(node2.center);
    	while (t <= 1) {
    		arc.add(new Vec3d(lastPoint.x, lastPoint.y, lastPoint.z));
    		// 1 Newton iteration to find the dt needed to achieve STEP_SIZE of displacement
    		newPoint = bezierPoint(controls, t + dt);
    		dt *= STEP_SIZE/VectorManip.getVec3dDist(lastPoint, newPoint);
    		newPoint = bezierPoint(controls, t + dt);
    		lastPoint = newPoint;
    		t += dt;
    	}
    	t = 1;
    	newPoint = bezierPoint(controls, t);
		arc.add(new Vec3d(newPoint.x, newPoint.y, newPoint.z));
    	return arc;
    }

    private void calculateArcs() {
    	arcs.clear();
    	for(int i = 0; i < nodes.size() - 1; i++) {
    		arcs.add(i, bezierArc(nodes.get(i), nodes.get(i + 1)));
    	}
    	if (isLoop == true) {
    		arcs.add(nodes.size() - 1, bezierArc(nodes.getLast(), nodes.getFirst()));
    	}
    }

	@Override
	public
	String getName() {
		return "Bezier Curve";
	}

	@Override
	ArrayList<String> getParamNames() {
		return null;
	}

	@Override
	int getParamCount() {
		return 0;
	}

	@Override
	public
	void defParams() {
		nodes.clear();
		this.showNodes = true;
		this.editing = false;
		this.moving = false;
		this.isLoop = false;
		this.reverse = false;
		this.currentIndex = NULL_SELECT;
	}

	public void exitEdit() {
		this.editing = false;
	}
	
	public void startEdit() {
		this.editing = true;
		this.showNodes = true;
	}
	
	@Override
	void setupUI() {
		final int midX = gui.width/2 - 40;
		final int midY = gui.height/2 - 20;
		
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addButton(BUTTON_NEW, "Start New Curve", 		gui.modeButtonWidth + 10, 30, gui.modeButtonWidth + 110, 50);
		gui.addButton(BUTTON_SHOW, "Show Nodes: " + showNodes, 	gui.width - 350, 30, gui.width - 250, 50);
		
		if (!this.nodes.isEmpty()) {
			gui.disableButton(BUTTON_NEW);
			gui.addButton(BUTTON_CLOSE, "Closed: " + isLoop, 	gui.modeButtonWidth + 115, 30, gui.modeButtonWidth + 195, 50);
			gui.addButton(BUTTON_SEL, "Select Node", 			midX - 40, midY - 10, midX + 40, midY + 10);
			
			gui.addButton(BUTTON_SELNEXT, "Select Next", 		midX + 40, midY - 10, midX +120, midY + 10);
			gui.addButton(BUTTON_SELPREV, "Select Prev",		midX -120, midY - 10, midX - 40, midY + 10);
			if (currentIndex != NULL_SELECT) {
				
				gui.addButton(BUTTON_MOV, "Move Here", 			midX - 40, midY - 35, midX + 40, midY - 15); // unimplemented
				gui.addButton(BUTTON_INSPRE, "Insert Before", 	midX -120, midY + 15, midX - 40, midY + 35);
				gui.addButton(BUTTON_INSPOST, "Insert After", 	midX + 40, midY + 15, midX +120, midY + 35);
				gui.addButton(BUTTON_DEL, "Remove", 			midX - 40, midY + 15, midX + 40, midY + 35);
				
				if (this.editing == false)
					gui.addButton(BUTTON_EDIT, "Edit Selected",	midX - 40, midY + 40, midX + 40, midY + 60);
				else {
					gui.addButton(BUTTON_CONF, "Confirm", 		midX - 40, midY + 40, midX + 40, midY + 60);
					gui.addButton(BUTTON_CANCEL, "Cancel", 		midX - 40, midY + 65, midX + 40, midY + 85);
				}
			}
				
			if (editing == true) {
				gui.disableButton(BUTTON_SEL);
				gui.disableButton(BUTTON_INSPRE);
				gui.disableButton(BUTTON_INSPOST);
				gui.disableButton(BUTTON_SELNEXT);
				gui.disableButton(BUTTON_SELPREV);
				gui.disableButton(BUTTON_MOV);
				gui.disableButton(BUTTON_EDIT);
			}
		}
		
	}

	@Override
	void handleButtonInput(int buttonId) {
		switch (buttonId) {
		case BUTTON_NEW:
			if(nodes.isEmpty()) {
				Vec3d center = new Vec3d(GuidesMod.currentBlock);
				nodes.add(new BezierNode(center, new Vec3d(0, 0, 0)));
				currentIndex = 0;
				System.out.println("Creating node at: " + center.toString());
				startEdit();
			}
			break;
		case BUTTON_INSPOST:
			Vec3d center = new Vec3d(GuidesMod.currentBlock);
			nodes.add(currentIndex + 1, (new BezierNode(center, new Vec3d(0, 0, 0))));
			currentIndex++;
			startEdit();
			break;
		case BUTTON_INSPRE:
			Vec3d center2 = new Vec3d(GuidesMod.currentBlock);
			nodes.add(currentIndex, (new BezierNode(center2, new Vec3d(0, 0, 0))));
			startEdit();
			break;
		case BUTTON_CONF:
			if (editing == true) {
				exitEdit();
			}
			break;
		case BUTTON_EDIT:
			if (editing != true && currentIndex != NULL_SELECT) {
				showNodes = true;
				// make backup
				old_node = new BezierNode(nodes.get(currentIndex));
				startEdit();
			}
			break;
		case BUTTON_SEL:
			currentIndex = getNodeIndex(new Vec3d(GuidesMod.currentBlock));
			break;
		case BUTTON_SELNEXT:
			if (currentIndex == NULL_SELECT) {
				currentIndex = 0;
			}
			if (currentIndex < nodes.size() - 1)
				currentIndex++;
			else if (isLoop == true)
				currentIndex = 0;
			break;
		case BUTTON_SELPREV:
			if (currentIndex == NULL_SELECT) {
				currentIndex = nodes.size()-1;
			}
			if (currentIndex > 0)
				currentIndex--;
			else if (isLoop == true)
				currentIndex = nodes.size()-1;
			break;
		case BUTTON_DEL:
			if (currentIndex != NULL_SELECT && currentIndex >= 0) {
				if (currentIndex >= nodes.size()) {
					currentIndex = nodes.size()-1;
				}
				nodes.remove(currentIndex);
				if (currentIndex >= nodes.size()) {
					currentIndex = nodes.size()-1;
				}
				exitEdit();
			}
			break;
		case BUTTON_SHOW:
			showNodes = showNodes ? false : true;
			break;
			
		case BUTTON_CANCEL:
			exitEdit();
			nodes.set(currentIndex, old_node);
			break;
		case BUTTON_CLOSE:
			isLoop = isLoop ? false : true;
			break;
		case BUTTON_MOV:
			if (currentIndex != NULL_SELECT && currentIndex >= 0) {
				nodes.get(currentIndex).reposition(GuidesMod.currentBlock);
			}
			break;
		case BUTTON_FLIP:
			reverse = reverse ? false : true;
			break;
		}
		
		//safety check
		if (currentIndex >= nodes.size() || currentIndex < 0)
			currentIndex = NULL_SELECT;
		
		gui.updateUI();
		calculateArcs();
		for (int i = 0; i < nodes.size(); i++) {
			if (i == currentIndex) {
				nodes.get(i).select();
			}
			else {
				nodes.get(i).deselect();
			}
		}
	}

	@Override
	protected ArrayList<Vec3d> generatePattern() {
		ArrayList<Vec3d> pattern = new ArrayList();
		for (ArrayList<Vec3d> arc : arcs) {
			for(int i = 0; i < arc.size(); i++) {
				pattern.add(arc.get(i).subtract(0.5,0.5,0.5));
			}
		}
		return pattern;
	}


	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideBezier guide = new GuideBezier(this.index, new BlockPos(0,0,0), GuidesMod.RED, GuidesMod.GREEN, GuidesMod.BLUE, GuidesMod.ALPHA, generatePattern());
		guide.isLoop = this.isLoop;
		guide.nodes = copyNodeList(nodes);
		return guide;
	}

	@Override
	public void loadPattern(Guide guide) {
		if (guide instanceof GuideBezier) {
			GuideBezier bezierGuide = (GuideBezier) guide;
			defParams();
			this.nodes = copyNodeList(bezierGuide.nodes);
			this.isLoop = bezierGuide.isLoop;
		}
	}

	@Override
	void renderEvent() {
		if((showNodes == true || editing == true) && !nodes.isEmpty()) {
			/* the node editing animation */
			if (editing == true) {
				nodes.get(currentIndex).setPosthandle(GuidesMod.player.getPositionVector().addVector(-0.5, -0.5, -0.5));
				nodes.get(currentIndex).rectify();
				calculateArcs();
			}
			for (BezierNode n : nodes)
				n.drawNode();
		}
		drawCurve();
	}
	
	@Override
	void renderUiEvent() {
		String text;
		int color;
		if (currentIndex != NULL_SELECT && !nodes.isEmpty()) {
			color = 0x87FF8F;
			text = new String("Selected node " + Integer.toString(currentIndex+1) + " of " + nodes.size() + " nodes");
		}
		else {
			color = 0xff6666;
			text = new String("No nodes selected");
		}
		gui.drawCenteredString(fr, "Look at the center block of a node then press Select to select", gui.width/2 - 40, 60, 0xffffff);
		gui.drawCenteredString(fr, text, gui.width/2 - 40, 50, color);
	}

	private void drawCurve() {
		for (ArrayList<Vec3d> arc : arcs) {
			for(int i = 0; i < arc.size() - 1; i++) {
				GuidesMod.drawLineOnWorld(GuidesMod.player, arc.get(i), arc.get(i+1), GuidesMod.partialTicks, 1, 1, 0, 1);
			}
		}
	}
	
	private BezierNode searchNode(Vec3d origin) {
		for (BezierNode node : nodes) {
			if (node.origin.equals(origin)) {
				return node;
			}
		}
		return null;
	}
	
	// returns 404 if node not found
	private int getNodeIndex(Vec3d origin) {
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.get(i).origin.equals(origin)) {
				return i;
			}
		}
		return NULL_SELECT;
	}
	
	private LinkedList<BezierNode> copyNodeList(LinkedList<BezierNode> list){
		LinkedList<BezierNode> newList = new LinkedList();
		for (BezierNode n : list) {
			newList.add(new BezierNode(n));
		}
		return newList;
	}

}