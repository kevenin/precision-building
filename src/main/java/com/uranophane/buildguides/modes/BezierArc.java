package com.uranophane.buildguides.modes;

import java.util.ArrayList;

import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class BezierArc extends Mode{

	public BezierArc() {
		super();
		defParams();
	}
	
	 private static Vec3d line2P(Vec3d start, Vec3d end, double t){
	        Vec3d newPos = new Vec3d(0,0,0);

	        newPos = newPos.add(start);
	        end = end.subtract(start);
	        end = end.scale(t);
	        newPos = newPos.add(end);

	        return newPos;
	 }

    protected static Vec3d bezierArc(ArrayList<Vec3d> points, double t){
        if (t < 0 || t > 1) t = t % 1;
        ArrayList<Vec3d> reducedPoints = new ArrayList<>();
        if (points.size() == 1) return points.get(0);
        for (int i = 1; i < points.size(); i++) {
            Vec3d p1 = points.get(i - 1);
            Vec3d p2 = points.get(i);
            reducedPoints.add(line2P(p1, p2, t));
        }
        return bezierArc(reducedPoints, t);
    }
    
    public void renderNodes() {
    	
    }

	@Override
	int getIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	ArrayList<String> getParamNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	int getParamCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	void defParams() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void setupUI() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void handleButtonInput(int buttonId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void collectInput() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected ArrayList<Vec3d> generatePattern() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	void loadTempParams() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadPattern(Guide guide) {
		// TODO Auto-generated method stub
		
	}

}

class BezierNode{
	Vec3d center;
	Vec3d prehandle;
	Vec3d posthandle;
	
}