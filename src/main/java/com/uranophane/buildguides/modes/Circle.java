package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideCircle;
import com.uranophane.buildguides.guides.GuideEllipsoid;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.util.NumberManip;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Circle extends Mode{
	private final int RADIUS = 0;	//id for radius parameter
	private final double STEP_SIZE = 0.33f;	//step size for circle generation
	
	private double radius = 0D;
	
	public Circle(SettingsGui parent) {
		super(parent);
		defParams();
	}
	
	@Override
	public
	String getName() {
		return "Circle";
	}

	@Override
	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add("Radius");
		return paramNames;
	}

	@Override
	int getParamCount() {
		return 1;
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(RADIUS, getParamNames().get(RADIUS), 10, 40, 50, Double.toString(radius), 7);
	}

	@Override
	void collectInput() {
		radius = NumberManip.stringToDouble_s(gui.modeFieldList.get(RADIUS).getText());
	}


	@Override
	protected ArrayList<Vec3d> generatePattern() {
		Set<Vec3d> pattern = new HashSet();
		double radius = this.radius;
		
		// uses the sqrt(r^2-x^2) method. x starts at 0, y starts at the maximum, r.
		double x = 0;
		double y = radius;
		
		//draws octant from 0 until y<=x.
		do {
			if (radius <= 1)
				break;
			//add (x,y) to the vector list as (x, 0, y)
			pattern.add(new Vec3d(x, 0, y)); //repeat for all 8 octants
			pattern.add(new Vec3d(-x, 0, y)); 
			pattern.add(new Vec3d(x, 0, -y)); 
			pattern.add(new Vec3d(-x, 0, -y)); 
			pattern.add(new Vec3d(y, 0, x)); 
			pattern.add(new Vec3d(-y, 0, x)); 
			pattern.add(new Vec3d(y, 0, -x)); 
			pattern.add(new Vec3d(-y, 0, -x)); 
			x += STEP_SIZE;
			y = Math.sqrt(Math.pow(radius, 2D) - Math.pow(x, 2D)); //y = sqrt( r^2 - x^2 )
		}
		while(!(y < x));
		
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}


	@Override
	public
	void defParams() {
		radius = 0D;
	}


	@Override
	void displayParams() {
		gui.modeFieldList.get(RADIUS).setText(Double.toString(radius));
	
	}

	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideCircle guide = new GuideCircle(this.index, origin, red, green, blue, alpha, this.generatePattern());
		guide.radius = radius;
		return guide;
	}

	@Override
	public void loadPattern(Guide guide) {
		if (guide instanceof GuideCircle) {
			GuideCircle circleGuide = (GuideCircle) guide;
			this.radius = circleGuide.radius;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}

}

