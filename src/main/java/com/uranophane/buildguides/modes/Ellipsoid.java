package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.guides.GuideEllipsoid;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Ellipsoid extends Mode{
	private static final double STEP_SIZE = 0.5d;	//step size for sampling
	private static final int X_RAD = 0;	//ids for directional radius parameters
	private static final int Y_RAD = 1;
	private static final int Z_RAD = 2;
	
	private double x_radius = 0D;
	private double y_radius = 0D;
	private double z_radius = 0D;
	
	public Ellipsoid(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public
	String getName() {
		return "Ellipsoid";
	}

	@Override
	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add(X_RAD, "X-Radius");
		paramNames.add(Y_RAD, "Y-Radius");
		paramNames.add(Z_RAD, "Z-Radius");
		return paramNames;
	}

	@Override
	int getParamCount() {
		return 3;
	}

	@Override
	public
	void defParams() {
		x_radius = 0D;
		y_radius = 0D;
		z_radius = 0D;
	}

	@Override
	void setupUI() {
		//int id, String label, x, y, w, String initVal, int maxLen
		gui.addParamField(X_RAD, getParamNames().get(X_RAD), 10, 40, 50, Double.toString(x_radius), 5);
		gui.addParamField(Y_RAD, getParamNames().get(Y_RAD), 10, 120, 50, Double.toString(y_radius), 5);
		gui.addParamField(Z_RAD, getParamNames().get(Z_RAD), 10, 80, 50, Double.toString(z_radius), 5);
	}

	@Override
	void handleButtonInput(int buttonId) {
	}

	@Override
	void collectInput() {

		x_radius = (getParamFieldById(X_RAD).getText() != null && !getParamFieldById(X_RAD).getText().isEmpty() && getParamFieldById(X_RAD).getText() != ".") ? 
				Float.parseFloat(getParamFieldById(X_RAD).getText()):0F;
		
		y_radius = (getParamFieldById(Y_RAD).getText() != null && !getParamFieldById(Y_RAD).getText().isEmpty() && getParamFieldById(Y_RAD).getText() != ".") ? 
				Float.parseFloat(getParamFieldById(Y_RAD).getText()):0F;
		
		z_radius = (getParamFieldById(Z_RAD).getText() != null && !getParamFieldById(Z_RAD).getText().isEmpty() && getParamFieldById(Z_RAD).getText() != ".") ? 
				Float.parseFloat(getParamFieldById(Z_RAD).getText()):0F;
	}

	
	private Set<Vec3d> appendVec3dOctuple(Set<Vec3d> pattern, Vec3d vector){
		double x = vector.x;
		double y = vector.y;
		double z = vector.z;

		pattern.add(new Vec3d(x, y, z));
		pattern.add(new Vec3d(x, -y, z));
		pattern.add(new Vec3d(-x, y, z));
		pattern.add(new Vec3d(-x, -y, z));
		pattern.add(new Vec3d(x, y, -z));
		pattern.add(new Vec3d(x, -y, -z));
		pattern.add(new Vec3d(-x, y, -z));
		pattern.add(new Vec3d(-x, -y, -z));
		return pattern;
	}

	@Override
	protected ArrayList<Vec3d> generatePattern() {

		//use set to prevent duplicates
		Set<Vec3d> pattern = new HashSet();
		double A = x_radius;
		double B = y_radius;
		double C = z_radius;

		
		//sample the same relation 3 times from 3 directions: x^2 + y^2 + z^2 = 1
		for (int u = 0; u < 3; u++) {
			double a = 0D;
			double b = 0D;
			double c = 0D;
			switch (u) {
			case 0:	//evaluating for x, y: c*sqrt(1-(x/a)^2-(y/b)^2)
				a = A;
				b = B;
				c = C;
				break;
			case 1:	//evaluating for y, z: a*sqrt(1-(y/b)^2-(z/c)^2)
				a = B;
				b = C;
				c = A;
				break;
			case 2:	//evaluating for x, z: b*sqrt(1-(x/a)^2-(z/c)^2)
				a = A;
				b = C;
				c = B;
				break;
			}
			for (double i = 0; i <= Math.round(a); i += STEP_SIZE) {
				for (double j = 0; j <= Math.round(b); j += STEP_SIZE) {
					double k = 0;
					if (Double.isNaN(Math.sqrt(1 - Math.pow(i/a, 2F) - Math.pow(j/b, 2F)))){
						continue;
					} 
					else {
						k = c * Math.sqrt(1 - Math.pow(i/a, 2F) - Math.pow(j/b, 2F));
						if (k < c/2) {
							break;
						}
					}
					double x = 0;
					double y = 0;
					double z = 0;
					switch (u) {
					case 0:	//evaluating for x, y: c*sqrt(1-(x/a)^2-(y/b)^2)
						x = i;
						y = j;
						z = k;
						break;
					case 1:	//evaluating for y, z: a*sqrt(1-(y/b)^2-(z/c)^2)
						x = k;
						y = i;
						z = j;
						break;
					case 2:	//evaluating for x, z: b*sqrt(1-(x/a)^2-(z/c)^2)
						x = i;
						y = k;
						z = j;
						break;
					}
					pattern = appendVec3dOctuple(pattern, new Vec3d(x, y, z));
				}
			}
		}
		//formula: X: a*sqrt(1-(y/b)^2-(z/c)^2)
		
		//convert set to list
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}


	@Override
	void displayParams() {	//load the saved parameters back into the UI
		getParamFieldById(X_RAD).setText(Double.toString(x_radius));
		getParamFieldById(Y_RAD).setText(Double.toString(y_radius));
		getParamFieldById(Z_RAD).setText(Double.toString(z_radius));
	}

	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideEllipsoid guide = new GuideEllipsoid(this.index, origin, red, green, blue, alpha, this.generatePattern());
		guide.x_radius = this.x_radius;
		guide.y_radius = this.y_radius;
		guide.z_radius = this.z_radius;
		return guide;
	}

	@Override
	public void loadPattern(Guide guide) {
		if (guide instanceof GuideEllipsoid) {
			GuideEllipsoid ellipGuide = (GuideEllipsoid) guide;
			this.x_radius = ellipGuide.x_radius;
			this.y_radius = ellipGuide.y_radius;
			this.z_radius = ellipGuide.z_radius;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
}
