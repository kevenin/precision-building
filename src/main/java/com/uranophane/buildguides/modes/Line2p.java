package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uranophane.buildguides.GuidesMod;
import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideEllipsoid;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Line2p extends Mode{
	// constants for interfacing with the UI
	private static final String NAME = "2-Point Line";
	private static final int ID = 7;
	private static final int BUTTON_POINT1 	= 0;
	private static final int BUTTON_POINT2	= 1;
	private static final int MULT 			= 6;
	private static final double STEP_SIZE 	= 1d;
	
	// interface parameters
	private double scalar = 1D;
	private Vec3i point1;
	private Vec3i point2;

	public Line2p(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public int getIndex() {
		return 7;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add(0, "Point 1 x");
		paramNames.add(1, "Point 1 y");
		paramNames.add(2, "Point 1 z");
		paramNames.add(3, "Point 2 x");
		paramNames.add(4, "Point 2 y");
		paramNames.add(5, "Point 2 z");
		paramNames.add(6, "Length Multiplier");
		return paramNames;
	}

	@Override
	int getParamCount() {
		return 7;
	}

	@Override
	public void defParams() {
		scalar = 1d;
		point1 = new Vec3i(0,0,0);
		point2 = new Vec3i(0,0,0);
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(MULT, getParamNames().get(MULT), 10, 40, 50, Double.toString(scalar), 5);
		gui.addButton(BUTTON_POINT1, "Set Point 2", 90, 100, 160, 120);
		gui.addButton(BUTTON_POINT2, "Set Point 1", 90, 76, 160, 96);
	}

	@Override
	void handleButtonInput(int buttonId) {
		
		switch (buttonId) {
		case BUTTON_POINT1: 
			point1 = new Vec3i(GuidesMod.currentBlock.getX(), GuidesMod.currentBlock.getY(), GuidesMod.currentBlock.getZ());
			//System.out.println("Set point 1 to " + tempParams.get(POINT1_X) + ", " + tempParams.get(POINT1_Y) + ", " + tempParams.get(POINT1_Z));
			break;
		case BUTTON_POINT2:
			point2 = new Vec3i(GuidesMod.currentBlock.getX(), GuidesMod.currentBlock.getY(), GuidesMod.currentBlock.getZ());
			break;
		}
	}

	@Override
	void collectInput() {
		scalar = (getParamFieldById(MULT).getText() != null && !getParamFieldById(MULT).getText().isEmpty() && getParamFieldById(MULT).getText() != ".") ? 
				Double.parseDouble(getParamFieldById(MULT).getText()) : 0D;
	}


	@Override
	protected ArrayList<Vec3d> generatePattern() {
		Set<Vec3d> pattern = new HashSet();
		double t = 0F;
		final Vec3d startAbs= new Vec3d(point1);
		final Vec3d endAbs 	= new Vec3d(point2);
		
		final Vec3d start	= new Vec3d(0, 0, 0);
		final Vec3d end		= endAbs.subtract(startAbs);
		final Vec3d middle	= end.scale(1/2d);
		final Vec3d dirn 	= end.normalize();
		double max_t = start.distanceTo(end) * scalar;
		
		for (t = 0F; t <= max_t / 2; t += STEP_SIZE) {
			pattern.add(dirn.scale(t).add(middle));
			pattern.add(dirn.scale(-t).add(middle));
		}
		
		ArrayList<Vec3d> patternArray = new ArrayList(pattern);
		return patternArray;
	}


	@Override
	void displayParams() {
		getParamFieldById(MULT).setText(Double.toString(scalar));
	}

	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideLine2p guide = new GuideLine2p(this.index, origin, red, green, blue, alpha, this.generatePattern());
		guide.point1 = new Vec3d(point1);
		guide.point2 = new Vec3d(point2);
		guide.scalar = this.scalar;
		return guide;
	}

	@Override
	public void loadPattern(Guide guide) {
		if (guide instanceof GuideLine2p) {
			GuideLine2p lineGuide = (GuideLine2p) guide;
			this.point1 = new Vec3i(lineGuide.point1.x, lineGuide.point1.y, lineGuide.point1.z);
			this.point2 = new Vec3i(lineGuide.point2.x, lineGuide.point2.y, lineGuide.point2.z);
			this.scalar = lineGuide.scalar;
	
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
	
}
