package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Set;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.LabelledTextField;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

// modes are UI renderers and input handlers. Their ultimate goal is to produce guide objects
public abstract class Mode{
	public int index;
	public FontRenderer fr;
	protected SettingsGui gui;

	public void setIndex(int index) {
		this.index = index;
	}
	
	public Mode(SettingsGui parent) {
		this.gui = parent;
	}
	
	
	public static ArrayList<Vec3i> appendVectorNoRepeat(ArrayList<Vec3i> pattern, Vec3i vector){	//single version
		if (!pattern.contains(vector)) {
			pattern.add(vector);
		}
		
		return pattern;
	}
	
	public static ArrayList<Vec3i> appendVectorsNoRepeat(ArrayList<Vec3i> pattern, ArrayList<Vec3i> appendee){	//array version
		for(int i = 0; i < appendee.size(); i++) {
			appendVectorNoRepeat(pattern, appendee.get(i));
		}
		return pattern;
	}
	
	public void setFR(FontRenderer fr) {
		this.fr = fr;
	}
	
	//none of these functions belong here
	
	
	public LabelledTextField getParamFieldById(int id) {
		for (int i = 0; i < gui.modeFieldList.size(); i++) {
			if (gui.modeFieldList.get(i).getId() == id) {
				return gui.modeFieldList.get(i);
			}
		}
		return null;
	}
	
	//this function does not belong here
	public void renderLabels() {
		for (int i = 0; i < gui.modeFieldList.size(); i++) {
			gui.drawString(fr, gui.modeFieldList.get(i).name + ": ", gui.modeFieldList.get(i).x, gui.modeFieldList.get(i).y - fr.FONT_HEIGHT - 2, 16777215);
		}
	}
	
	public int getIndex() {
		return index;
	}
	public abstract String getName();
	abstract ArrayList<String> getParamNames();
	abstract int getParamCount();
	public abstract void defParams();
	abstract void setupUI();
	void handleButtonInput(int buttonId) {}
	void collectInput() {}
	protected abstract ArrayList<Vec3d> generatePattern();
	void displayParams() {}
	
	void renderEvent() {}
	void renderUiEvent() {}
	
	// guide constructor. Needs input from main class. Returns deep copy
	public abstract Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha);
	public abstract void loadPattern(Guide guide);


}
