package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class ModeObjController extends SettingsGui{

	public static ArrayList<Mode> modeList = new ArrayList();
	
	public static void init() {
		modeList.add(new Rays());					// instantiate every mode
		modeList.add(new Circle());
		modeList.add(new Ellipsoid());
		modeList.add(new Line2p());
		//modeList.add(new BezierArc());
		
		for (int i = 0; i < modeList.size(); i++) {	//assign each mode an ID
			modeList.get(i).setIndex(i);
		}
			
	}
	
	public static Mode getMode(int index) {
		return modeList.get(index);
	}
	
	public static Mode getCurrentMode() {
		return modeList.get(modeId);
	}
	
	public static int getModeCount() {
		return modeList.size();
	}
	
	public static String getModeName(int index){
		return modeList.get(index).getName();
	}
	
	public static ArrayList<String> getParamNames(int index){
		return modeList.get(index).getParamNames();
	}
	
	public static ArrayList<Vec3d> generateCurrentPattern(){
		return modeList.get(modeId).generatePattern();
	}
	
	public static void setupCurrentUI() {
		modeList.get(modeId).setupUI();
	}
	
	public static void paramButtonPress(int buttonId) {
		modeList.get(modeId).handleButtonInput(buttonId);
	}
	
	public static void collectInput() {
		modeList.get(modeId).collectInput();
	}

	public static void loadPattern(Guide guide) {
		modeList.get(modeId).loadPattern(guide);
	}

	
	public static void loadTempParams() {
		modeList.get(currentModeButtonId - 1000).loadTempParams();
	}

	public static void setFR(FontRenderer fr) {
		modeList.get(currentModeButtonId - 1000).setFR(fr);
	}
	
	public static void renderText() {
		modeList.get(currentModeButtonId - 1000).renderText();
	}
}
