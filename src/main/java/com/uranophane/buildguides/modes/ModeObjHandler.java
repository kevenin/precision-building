package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.guides.Guide;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class ModeObjHandler{

	public static ArrayList<Mode> modeList = new ArrayList();
	private SettingsGui gui;
	
	public ModeObjHandler(SettingsGui gui) {
		this.gui = gui;
		this.init();
	}
	
	private void init() {
		modeList.add(new Rays(this.gui));					// instantiate every mode
		modeList.add(new Circle(this.gui));
		modeList.add(new Ellipsoid(this.gui));
		modeList.add(new Line2p(this.gui));
		modeList.add(new Bezier(this.gui));
		
		for (int i = 0; i < modeList.size(); i++) {	//assign each mode an ID
			modeList.get(i).setIndex(i);
		}
			
	}
	
	public Mode getMode(int index) {
		return modeList.get(index);
	}
	
	public Mode getCurrentMode() {
		return modeList.get(gui.getModeId());
	}
	
	public int getModeCount() {
		return modeList.size();
	}
	
	public String getModeName(int index){
		return modeList.get(index).getName();
	}
	
	public ArrayList<String> getParamNames(int index){
		return modeList.get(index).getParamNames();
	}
	
	public ArrayList<Vec3d> generateCurrentPattern(){
		return modeList.get(gui.getModeId()).generatePattern();
	}
	
	public void setupCurrentUI() {
		modeList.get(gui.getModeId()).setupUI();
	}
	
	public void paramButtonPress(int buttonId) {
		modeList.get(gui.getModeId()).handleButtonInput(buttonId);
	}
	
	public void collectInput() {
		modeList.get(gui.getModeId()).collectInput();
	}

	public void loadPattern(Guide guide) {
		modeList.get(gui.getModeId()).loadPattern(guide);
	}

	
	public void loadTempParams() {
		modeList.get(gui.getModeId()).displayParams();
	}

	public void setFR(FontRenderer fr) {
		modeList.get(gui.getModeId()).setFR(fr);
	}
	
	public void renderUiEvent() {
		modeList.get(gui.getModeId()).renderLabels();
		modeList.get(gui.getModeId()).renderUiEvent();
	}

	public void renderEvent() {
		for(Mode m : modeList) {
			m.renderEvent();
		}
	}

}
