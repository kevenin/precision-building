package com.uranophane.buildguides.modes;

import java.util.ArrayList;
import java.util.Collections;

import com.uranophane.buildguides.gui.SettingsGui;
import com.uranophane.buildguides.gui.textfields.UnsignedFloatField;
import com.uranophane.buildguides.gui.uiLists.UiGuideEntry;
import com.uranophane.buildguides.guides.GuideLine2p;
import com.uranophane.buildguides.guides.Guide;
import com.uranophane.buildguides.guides.GuideCircle;
import com.uranophane.buildguides.guides.GuideRays;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class Rays extends Mode{
	private final int LENGTH = 0;	//id for length parameter
	private final int SPOKES = 1;	//id for points parameter (unused)
	
	private int spokes = 4;
	private double length = 0D;
	
	public Rays(SettingsGui parent) {
		super(parent);
		defParams();
	}

	@Override
	public
	String getName() {
		return "Star";
	}

	@Override
	ArrayList<String> getParamNames() {
		ArrayList<String> paramNames = new ArrayList();
		paramNames.add("Length");
		paramNames.add("Spokes");
		return paramNames;
	}

	@Override
	int getParamCount() {
		return 2;
	}

	@Override
	void setupUI() {
		//int id, String label, int x, int y, int w, String initVal, int maxLen
		gui.addParamField(LENGTH, getParamNames().get(LENGTH), 10, 40, 50, Double.toString(length), 5);
		gui.addParamField(SPOKES, getParamNames().get(SPOKES), 10, 80, 50, Integer.toString(spokes), 3);
	}

	@Override
	void collectInput() {
		length =  (getParamFieldById(LENGTH).getText() != null && 
				!gui.modeFieldList.get(LENGTH).getText().isEmpty() && 
				gui.modeFieldList.get(LENGTH).getText() != ".") ? //is the parameter box empty?
				Double.parseDouble(getParamFieldById(LENGTH).getText()) : 0F;
				
		spokes = (getParamFieldById(SPOKES).getText() != null && 
				!gui.modeFieldList.get(SPOKES).getText().isEmpty() && 
				gui.modeFieldList.get(SPOKES).getText() != ".") ? 
				(int)Double.parseDouble(getParamFieldById(SPOKES).getText()) : 0;
	}



	@Override
	void handleButtonInput(int buttonId) {
	}

	@Override
	protected ArrayList<Vec3d> generatePattern() {
		ArrayList<Vec3d> pattern = new ArrayList();
		long size 		= Math.round(length);
		int symmetry 	= spokes;
		double full_circle = 2 * Math.PI;
		double angle	= 0d;
		
		//grow size times in each symmetry direction

		for (int i = 1; i <= size; i++) {						//first loop is radius
			for (int j = 0; j < symmetry; j++) {				//second loop is n-sector
				angle = j * full_circle / symmetry;
				pattern.add(new Vec3d(Math.cos(angle), 0, Math.sin(angle)).scale(i));
			}
		}
		return pattern;
	}

	@Override
	public
	void defParams() {
		length = 16D;
		spokes = 4;
	}

	@Override
	void displayParams() {
		getParamFieldById(LENGTH).setText(Double.toString(length));
		getParamFieldById(SPOKES).setText(Integer.toString(spokes));
	}


	@Override
	public Guide getPattern(BlockPos origin, float red, float green, float blue, float alpha) {
		GuideRays guide = new GuideRays(this.index, origin, red, green, blue, alpha, this.generatePattern());
		guide.length = this.length;
		guide.spokes = this.spokes;
		return guide;
	}

	@Override
	public void loadPattern(Guide guide) {
		if (guide instanceof GuideRays) {
			GuideRays raysGuide = (GuideRays) guide;
			this.length = raysGuide.length;
			this.spokes = raysGuide.spokes;
		}
	}

	@Override
	void renderEvent() {
		// TODO Auto-generated method stub
		
	}
}
