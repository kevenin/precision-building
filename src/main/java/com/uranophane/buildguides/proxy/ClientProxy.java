package com.uranophane.buildguides.proxy;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.uranophane.buildguides.Reference;
import com.uranophane.buildguides.cmd.CmdColor;
import com.uranophane.buildguides.cmd.CmdDebug;
import com.uranophane.buildguides.cmd.CmdHelp;
import com.uranophane.buildguides.cmd.CmdPlace;
import com.uranophane.buildguides.cmd.CmdSel;
import com.uranophane.buildguides.cmd.CmdVis;
import com.uranophane.buildguides.cmd.CmdXray;
import com.uranophane.buildguides.handlers.RegistryHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.command.CommandBase;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy{

	
	public void preInit(FMLPreInitializationEvent event) {
		
		super.preInit(event);
	};
	
	public void registerTickHandler() {
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	public static HashMap<String, KeyBinding> keyBinds = new HashMap();
	public static CommandBase[] commands = {new CmdHelp(), new CmdXray(), new CmdPlace(), new CmdColor(), new CmdSel(), new CmdVis(), new CmdDebug()};
	
	public void init(FMLInitializationEvent event) {
		
		super.init(event);
		RegistryHandler.Client();
		
		keyBinds.put("place", new KeyBinding("Place Guide",Keyboard.KEY_R,"Precision Building"));
		keyBinds.put("menu", new KeyBinding("Open Menu",Keyboard.KEY_G,"Precision Building"));
		keyBinds.put("special",  new KeyBinding("Special Action",Keyboard.KEY_E,"Precision Building"));
		
		for (KeyBinding kb : keyBinds.values()) {
			ClientRegistry.registerKeyBinding(kb);

		}
		
		for (int i = 0; i < commands.length; ++i) {
			ClientCommandHandler.instance.registerCommand(commands[i]);

		}
	};
	

	public void postInit(FMLPostInitializationEvent event) {
		
		super.postInit(event);
	};
}
