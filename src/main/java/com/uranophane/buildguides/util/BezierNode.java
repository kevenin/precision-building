package com.uranophane.buildguides.util;

import org.lwjgl.opengl.GL11;

import com.uranophane.buildguides.GuidesMod.FadeMode;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class BezierNode{
	public Vec3d origin;
	public Vec3d center;
	public Vec3d prehandle;
	public Vec3d posthandle;
	public double prelength;
	public double postlength;
	private boolean isSelected;
	public BezierNode(Vec3d center, Vec3d post) {
		this.origin = center;
		this.center = origin.addVector(0.5, 0.5, 0.5);
		this.posthandle = new Vec3d(Math.round(post.x), Math.round(post.y), Math.round(post.z)).addVector(0.5,0.5,0.5);
		this.prehandle = this.origin.subtract(post);
	}
	
	public BezierNode(BezierNode node) {
		this.origin = node.origin;
		this.center = node.center;
		this.posthandle = node.posthandle;
		this.prehandle = node.prehandle;
	}
	
	public void drawNode() {
		BlockPos centerBlock = new BlockPos(center);
		BlockPos preBlock = new BlockPos(prehandle);
		BlockPos postBlock = new BlockPos(posthandle);
		
		GlStateManager.depthFunc(GL11.GL_ALWAYS);
		GlStateManager.enableDepth();
		RenderTools.setTempRenderDist(128);
		RenderTools.drawBoxOnWorld(centerBlock, 0, 0, 1, 1);
		if (!isSelected) {
			RenderTools.drawLineOnWorld(prehandle, posthandle, 0, 0, 1, 1);
			RenderTools.drawPointOnWorld(prehandle, 150, 0, 0, 1, 1, FadeMode.SIZE_FADE);
			RenderTools.drawPointOnWorld(posthandle, 150, 0, 0, 1, 1, FadeMode.SIZE_FADE);
		}
		else {
			RenderTools.drawCubeOnWorld(centerBlock, 0F, 0.7F, 1F, 1F);
			RenderTools.drawLineOnWorld(prehandle, posthandle, 0F, 0.7F, 1F, 1F);
			RenderTools.drawPointOnWorld(posthandle, 150, 0.1F, 1F, 0.1F, 1, FadeMode.SIZE_FADE);
			RenderTools.drawPointOnWorld(prehandle, 150, 1F, 0.1F, 0.1F, 1, FadeMode.SIZE_FADE);
		}
		RenderTools.revertRenderDist();
		GlStateManager.depthFunc(GL11.GL_LEQUAL);
	}

	public void setPosthandle(Vec3d location) {
		this.posthandle = new Vec3d(location.x, location.y, location.z).addVector(0.5,0.5,0.5);
	}
	
	public void setOrigin(Vec3d location) {
		this.origin = location;
		this.center = origin.addVector(0.5, 0.5, 0.5);
	}
	
	public void setPrehandle(Vec3d location) {
		this.prehandle = location;
	}

	public void rectify() {
		this.prehandle = this.center.subtract(posthandle).add(center);
	}

	public void select() {
		this.isSelected = true;
	}
	
	public void deselect() {
		this.isSelected = false;
	}

	public void reposition(BlockPos currentBlock) {
		Vec3d delta = new Vec3d(currentBlock.getX() - origin.x, currentBlock.getY() - origin.y, currentBlock.getZ() - origin.z);
		this.origin = new Vec3d(currentBlock);
		this.center = origin.addVector(0.5, 0.5, 0.5);
		this.prehandle = prehandle.add(delta);
		this.posthandle = posthandle.add(delta);
	}

}