package com.uranophane.buildguides.util;

public class ColorManip {
	public static int RGBtoInt(int red, int green, int blue) {
		return blue + green * 256 + red * 256 * 256;
	}
	
	public static int RGBftoInt(float red, float green, float blue) {
		
		return RGBtoInt((int)(red * 255), (int)(green * 255), (int)(blue * 255));
	}
	
	public static RGBA randSatColor() {
		double hue = 2D * Math.PI * Math.random();
		RGBA color = new RGBA();

		color.red = Math.max(0, Math.min(1, Math.sin(hue) + 0.5));
		color.green = Math.max(0, Math.min(1, Math.sin(hue - 2D/3 * Math.PI) + 0.5));
		color.blue = Math.max(0, Math.min(1, Math.sin(hue - 4D/3 * Math.PI) + 0.5));
		
		return color;
	}
	
	
}
