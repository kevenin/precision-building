package com.uranophane.buildguides.util;

public class HSVA {
	double hue;
	double sat;
	double val;
	double alpha;
	
	public HSVA() {
		hue = 0;
		sat = 0;
		val = 0;
		alpha = 0;
	}
	
	public HSVA(double h, double s, double v, double a) {
		hue = h;
		sat = s;
		val = v;
		alpha = a;
	}
	
	public HSVA(RGBA rgba) {
		// TODO
	}
}
