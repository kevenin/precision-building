package com.uranophane.buildguides.util;

import java.util.LinkedList;
import java.util.Set;

import net.minecraft.util.math.Vec3d;

public class NumberManip {
	public static double stringToDouble_s(String text) {
		boolean hasDot = false;
		String string = "";
		for (char c : text.toCharArray()) {
			if (c == '.' && hasDot == false) {
				hasDot = true;
				string = string.concat(Character.toString(c));
			}
			else if (c == '.' && hasDot == true) {
				continue;
			}
			else
				string = string.concat(Character.toString(c));
		}
		text = string;
		
		try {
			return (text != null && !text.isEmpty() && text != ".") ? 
					Double.parseDouble(text)
					:0D;
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
}
