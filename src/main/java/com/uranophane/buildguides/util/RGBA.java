package com.uranophane.buildguides.util;

public class RGBA {
	public double red;
	public double green;
	public double blue;
	public double alpha;
	
	public RGBA() {
		this.red = 0d;
		this.green = 0d;
		this.blue = 0d;
		this.alpha = 1d;
	}
	
	public RGBA(int r, int g, int b, int a) {
		this.red = r;
		this.green = g;
		this.blue = b;
		this.alpha = a;
	}
	
	public RGBA(RGBA color) {
		this.red = color.red;
		this.green = color.green;
		this.blue = color.blue;
		this.alpha = color.alpha;
	}
	
	public RGBA(HSVA hsva) {
		// TODO
	}
	
	public void randomize(boolean r, boolean g, boolean b, boolean a) {
		red = r ? red : Math.random();
		green = g ? green : Math.random();
		blue = b ? blue : Math.random();
		alpha = a ? alpha : Math.random();
	}
	
	public HSVA toHSVA() {
		return new HSVA(this);
	}
}

