package com.uranophane.buildguides.util;

import org.lwjgl.opengl.GL11;

import com.uranophane.buildguides.GuidesMod.FadeMode;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public class RenderTools {

	private static final double MINSHRINK = 0.1D;

	public RenderTools() {
	}
	
	public static double rx = 0;
	public static double ry = 0;
	public static double rz = 0;
	private static double drawDist = 10;
	private static final int MAXTRACE = 150;
	private static final float ALPHA_THRESH = 0.14F;
	private static double memDrawDist = 10d;
	
	public static void setPartialCoords(double d0, double d1, double d2) {
		rx = d0;
		ry = d1;
		rz = d2;
	}
	
	public static void setRenderDist(double renderDist) {
		drawDist = renderDist;
		memDrawDist = drawDist;
	}
	
	public static void setTempRenderDist(double rd) {
		drawDist = rd;
	}
	
	public static void revertRenderDist() {
		drawDist = memDrawDist;
	}
	
	public static void drawLineOnWorld(Vec3d pos1, Vec3d pos2, float red, float green, float blue, float alpha) {
		
        
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.glLineWidth(12F/((float) (Math.sqrt(pos1.squareDistanceTo(rx, ry, rz)) + Math.sqrt(pos2.squareDistanceTo(rx, ry, rz)))/4F + 1.1F));
        GlStateManager.disableTexture2D();
        GlStateManager.depthMask(false);
        
        if (Math.sqrt(pos1.squareDistanceTo(rx, ry, rz)) <= drawDist || Math.sqrt(pos2.squareDistanceTo(rx, ry, rz)) <= drawDist) {
			GlStateManager.depthFunc(GL11.GL_GREATER); //draw underground part
			drawLine(pos1.x - rx, pos1.y - ry, pos1.z - rz, pos2.x - rx, pos2.y - ry, pos2.z - rz, red, green, blue,
					alpha / 4);
			GlStateManager.depthFunc(GL11.GL_LEQUAL); //draw aboveground part
			drawLine(pos1.x - rx, pos1.y - ry, pos1.z - rz, pos2.x - rx, pos2.y - ry, pos2.z - rz, red, green, blue,
					alpha);
		}
		GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
	}

	/**
	 *  Does NOT align with world!
	 *  Use GL matrix translation
	 */
	public static void pushBlockToVBO(BufferBuilder buffer, BlockPos blockpos, float red, float green, float blue, float alpha, boolean filled, FadeMode fadeMode) {
		
        AxisAlignedBB cubeBox;
        double distToBlockpos = Math.sqrt(blockpos.distanceSq(rx, ry, rz));
        if (distToBlockpos <= drawDist) {
        	switch (fadeMode) {
			case HARD_CULL:
				alpha = 1F;
				cubeBox = new AxisAlignedBB(blockpos).shrink(MINSHRINK);
				if (filled)
					pushTriStripCubeToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
				else
					pushLineStripBoxToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
				break;
				
			case ALPHA_SIZE_FADE: 	
		    
				//distance falloff truncated at 0
		        alpha *= (float)Math.exp(distToBlockpos * Math.log(ALPHA_THRESH) 
		        						/ drawDist);
		    //don't render if alpha is 0
		        if (alpha >= ALPHA_THRESH) {
		        //the AxisAlignedBB(BlockPos) constructor uses 1x1x1 as default size
		        	cubeBox = new AxisAlignedBB(blockpos).shrink(MINSHRINK + 0.35D * MathHelper.clamp(2 * (distToBlockpos/drawDist) - 1, 0d, 1d));
		        	if (filled)
						pushTriStripCubeToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
					else
						pushLineStripBoxToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
				}
		        break;
		        
			case ALPHA_FADE:
				alpha *= (float)Math.exp(distToBlockpos * Math.log(ALPHA_THRESH) 
						/ drawDist);
				if (alpha >= ALPHA_THRESH) {
					cubeBox = new AxisAlignedBB(blockpos).shrink(MINSHRINK);
					if (filled)
						pushTriStripCubeToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
					else
						pushLineStripBoxToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
				}
				break;
				
			case SIZE_FADE:
					cubeBox = new AxisAlignedBB(blockpos).shrink(MINSHRINK + 0.35D * MathHelper.clamp(2 * (distToBlockpos/drawDist) - 1, 0d, 1d));
					if (filled)
						pushTriStripCubeToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
					else
						pushLineStripBoxToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, alpha);
				break;
			default:
				break;
			}
        }
	}
	
	/**
	 *  Does NOT align with world!
	 *  Use GL matrix translation
	 *  Doesn't update in real time! 
	 *  Only call this when the geometry changes
	 */
	public static void pushCheapBlockToVBO(BufferBuilder buffer, BlockPos blockpos, float red, float green, float blue) {

        //the AxisAlignedBB(BlockPos) constructor uses 1x1x1 as default size
        AxisAlignedBB cubeBox = new AxisAlignedBB(blockpos).shrink(0.35F);
        pushLineStripBoxToBuffer(buffer, cubeBox.minX, cubeBox.minY, cubeBox.minZ, cubeBox.maxX, cubeBox.maxY, cubeBox.maxZ, red, green, blue, 0.2F);
	}
		
	public static void drawPointOnWorld(Vec3d pos, float size, float red, float green, float blue, float alpha, FadeMode fadeMode) {
	      
        Tessellator tess = Tessellator.getInstance();
		BufferBuilder buffer = tess.getBuffer();
		
		GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.disableTexture2D();
        GlStateManager.depthMask(false);
        
        double dist = Math.sqrt(pos.squareDistanceTo(rx, ry + 1, rz));
       
        GlStateManager.depthFunc(GL11.GL_LEQUAL);
        switch (fadeMode) {
		case ALPHA_FADE:
			GL11.glPointSize(size);
			alpha *= (float)Math.exp(dist * Math.log(ALPHA_THRESH) 
						/ drawDist);
			if (dist <= drawDist && alpha > ALPHA_THRESH) {
	        	buffer.begin(GL11.GL_POINTS, DefaultVertexFormats.POSITION_COLOR);
	        	buffer.pos(pos.x - rx, pos.y - ry, pos.z - rz).color(red, green, blue, alpha).endVertex();
	        	tess.draw();
		    }
			else
				alpha = 0;
			break;
		case ALPHA_SIZE_FADE:
			GL11.glPointSize((float)(size/dist));
			alpha *= (float)Math.exp(dist * Math.log(ALPHA_THRESH) 
					/ drawDist);
			if (dist <= drawDist && alpha > ALPHA_THRESH) {
	        	buffer.begin(GL11.GL_POINTS, DefaultVertexFormats.POSITION_COLOR);
	        	buffer.pos(pos.x - rx, pos.y - ry, pos.z - rz).color(red, green, blue, alpha).endVertex();
	        	tess.draw();
		    }
			else
				alpha = 0;
			break;
		case HARD_CULL:
			GL11.glPointSize(size);
			if (dist <= drawDist) {
	        	buffer.begin(GL11.GL_POINTS, DefaultVertexFormats.POSITION_COLOR);
	        	buffer.pos(pos.x - rx, pos.y - ry, pos.z - rz).color(red, green, blue, alpha).endVertex();
	        	tess.draw();
		    }
			else
				alpha = 0;
			break;
		case INVERSE_CULL:
			GL11.glPointSize(size);
			// TODO
			break;
		case SIZE_FADE:
			GL11.glPointSize((float) (size/dist));
			if (dist <= drawDist) {
	        	buffer.begin(GL11.GL_POINTS, DefaultVertexFormats.POSITION_COLOR);
	        	buffer.pos(pos.x - rx, pos.y - ry, pos.z - rz).color(red, green, blue, alpha).endVertex();
	        	tess.draw();
		    }
			else
				alpha = 0;
			break;
		default:
			break;
        }
        GlStateManager.depthFunc(GL11.GL_GREATER);
        buffer.begin(GL11.GL_POINTS, DefaultVertexFormats.POSITION_COLOR);
    	buffer.pos(pos.x - rx, pos.y - ry, pos.z - rz).color(red, green, blue, alpha / 4).endVertex();
    	tess.draw();
    	GlStateManager.depthFunc(GL11.GL_LEQUAL);
        GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.enableDepth();
        GlStateManager.disableBlend();
	}
	
	public static void drawBoxOnWorld(BlockPos centerBlock, float red, float green, float blue, float alpha) {
		Tessellator tess = Tessellator.getInstance();
		BufferBuilder buffer = tess.getBuffer();
		
		usingWireframeGlState(() -> {
			buffer.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION_COLOR);
			pushBlockToVBO(buffer, centerBlock, red, green, blue, alpha, false, FadeMode.HARD_CULL);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(-rx, -ry, -rz);
			tess.draw();
			GlStateManager.popMatrix();
		});
	}
	
	public static void drawCubeOnWorld(BlockPos centerBlock, float red, float green, float blue, float alpha) {
		Tessellator tess = Tessellator.getInstance();
		BufferBuilder buffer = tess.getBuffer();
		
		usingWireframeGlState(() -> {
			buffer.begin(GL11.GL_TRIANGLE_STRIP, DefaultVertexFormats.POSITION_COLOR);
			pushBlockToVBO(buffer, centerBlock, red, green, blue, alpha, true, FadeMode.ALPHA_FADE);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(-rx, -ry, -rz);
			tess.draw();
			GlStateManager.popMatrix();
		});
	}

	public static void drawLine(double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha){
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(3, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.pos(minX, minY, minZ).color(red, green, blue, 0.0F).endVertex();
		bufferbuilder.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
		bufferbuilder.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
		bufferbuilder.pos(maxX, maxY, maxZ).color(red, green, blue, 0.0F).endVertex();
		tessellator.draw();
	}

	public static void pushLineStripBoxToBuffer(BufferBuilder buffer, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha)
    {
		// initialization 0 = min, 1 = max
        buffer.pos(minX, minY, minZ).color(red, green, blue, 0.0F).endVertex();
        // 000
        buffer.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 100
        buffer.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 101
        buffer.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 001
        buffer.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 000
        buffer.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 010
        buffer.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 110
        buffer.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 111
        buffer.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        // 011
        buffer.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        // 010 start transparent transfer
        buffer.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 011 end transparent transfer
        buffer.pos(minX, maxY, maxZ).color(red, green, blue, 0.0F).endVertex();
        
        // 001 start transparent transfer
        buffer.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 111 end transparent transfer
        buffer.pos(maxX, maxY, maxZ).color(red, green, blue, 0.0F).endVertex();
        
        // 101 start transparent transfer
        buffer.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 110 end transparent transfer
        buffer.pos(maxX, maxY, minZ).color(red, green, blue, 0.0F).endVertex();
        
        // 100 start transparent transfer
        buffer.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 100 end transparent transfer
        buffer.pos(maxX, minY, minZ).color(red, green, blue, 0.0F).endVertex();
    }
	
	public static void pushTriStripCubeToBuffer(BufferBuilder buffer, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha)
    {
		// initialization 0 = min, 1 = max
        buffer.pos(minX, minY, minZ).color(red, green, blue, 0.0F).endVertex();
        // 000
        buffer.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 010
        buffer.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex(); 
        // 001
        buffer.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 011
        buffer.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        // 101
        buffer.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 111
        buffer.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        // 100
        buffer.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 110
        buffer.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 000 start transparent transfer
        buffer.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 010
        buffer.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex(); 

        // 010 end transparent transfer
        buffer.pos(minX, maxY, minZ).color(red, green, blue, 0.0F).endVertex();
        // 010 end transparent transfer
        buffer.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 011
        buffer.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        // 110
        buffer.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        // 111
        buffer.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();

        // 000 end transparent transfer
        buffer.pos(minX, minY, minZ).color(red, green, blue, 0.0F).endVertex();
        // 000 end transparent transfer
        buffer.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 001
        buffer.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        // 100
        buffer.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        // 101
        buffer.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
    }
	
	public static void usingBuffer(VertexBuffer buff, Runnable renderCode) {
		buff.bindBuffer();
		GlStateManager.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.glEnableClientState(GL11.GL_COLOR_ARRAY);
		GlStateManager.glVertexPointer(3, GL11.GL_FLOAT, 16, 0);
		GlStateManager.glColorPointer(4, GL11.GL_UNSIGNED_BYTE, 16, 12);
		renderCode.run();
		buff.unbindBuffer();
		GlStateManager.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.glDisableClientState(GL11.GL_COLOR_ARRAY);
	}
	
	public static void usingWireframeGlState(Runnable renderCode) {
		GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.disableTexture2D();
        GlStateManager.depthMask(false);
		renderCode.run();
		GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();
        GlStateManager.enableDepth();
        GlStateManager.disableBlend();
	}
}
