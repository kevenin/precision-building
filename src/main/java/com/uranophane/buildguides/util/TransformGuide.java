package com.uranophane.buildguides.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;

import org.lwjgl.util.vector.Matrix;

import com.uranophane.buildguides.cmd.CmdHelp;

import net.minecraft.util.math.Vec3d;

public class TransformGuide {
	private Matrix4d tf = new Matrix4d();
	private Stack<Matrix4d> tfStack = new Stack();
	protected Stack<String> tfRecord = new Stack();
	public Vec3d origin = new Vec3d(0, 0, 0);
	
	public TransformGuide() {
		tf.setIdentity();
	}
	
	public TransformGuide(TransformGuide tfg) {
		tf.set(tfg.tf);
	}

	/**
	 * Uses [T]*[R]*[T-1] to rotate about an arbitrary point
	 * @param ang
	 * @param origin
	 */
	public TransformGuide addRotX(double ang, Vec3d origin) {
		this.origin = origin;
		
		Matrix4d rotMatx = new Matrix4d();
		rotMatx.setIdentity();
		rotMatx.rotX(ang);
		
		Matrix4d shift1 = new Matrix4d();
		Vector3d shiftVec = new Vector3d(origin.x, origin.y, origin.z);
		shift1.setIdentity();
		shift1.setTranslation(shiftVec);
		
		Matrix4d shift2 = new Matrix4d();
		shiftVec = new Vector3d(-origin.x, -origin.y, -origin.z);
		shift2.setIdentity();
		shift2.setTranslation(shiftVec);
		
		shift1.mul(rotMatx);
		shift1.mul(shift2);
		tfStack.push(shift1);
		tfRecord.push("Pitch rotation " + Math.toDegrees(ang) + " degrees");
		calculateTf();
		return this;
	}
	
	public TransformGuide addRotY(double ang, Vec3d origin) {
		this.origin = origin;
		
		Matrix4d rotMatx = new Matrix4d();
		rotMatx.setIdentity();
		rotMatx.rotY(ang);
		
		Matrix4d shift1 = new Matrix4d();
		Vector3d shiftVec = new Vector3d(origin.x, origin.y, origin.z);
		shift1.setIdentity();
		shift1.setTranslation(shiftVec);
		
		Matrix4d shift2 = new Matrix4d();
		shiftVec = new Vector3d(-origin.x, -origin.y, -origin.z);
		shift2.setIdentity();
		shift2.setTranslation(shiftVec);
		
		shift1.mul(rotMatx);
		shift1.mul(shift2);
		tfStack.push(shift1);
		tfRecord.push("Yaw rotation " + Math.toDegrees(ang) + " degrees");
		calculateTf();
		return this;
	}
	
	public TransformGuide addRotZ(double ang, Vec3d origin) {
		this.origin = origin;
		
		Matrix4d rotMatx = new Matrix4d();
		rotMatx.setIdentity();
		rotMatx.rotZ(ang);
		
		Matrix4d shift1 = new Matrix4d();
		Vector3d shiftVec = new Vector3d(origin.x, origin.y, origin.z);
		shift1.setIdentity();
		shift1.setTranslation(shiftVec);
		
		Matrix4d shift2 = new Matrix4d();
		shiftVec = new Vector3d(-origin.x, -origin.y, -origin.z);
		shift2.setIdentity();
		shift2.setTranslation(shiftVec);
		
		shift1.mul(rotMatx);
		shift1.mul(shift2);
		tfStack.push(shift1);
		tfRecord.push("Roll rotation " + Math.toDegrees(ang) + " degrees");
		calculateTf();
		return this;
	}
	
	public void undoXform() {
		tfStack.pop();
		tfRecord.pop();
		calculateTf();
	}
	
	public TransformGuide addShift(double x, double y, double z) {
		Matrix4d transMatx = new Matrix4d();
		Vector3d trans = new Vector3d(x, y, z);
		transMatx.setIdentity();
		transMatx.setTranslation(trans);
		tfStack.push(transMatx);
		tfRecord.push("Shift by " + trans.toString());
		calculateTf();
		return this;
	}
	
	public TransformGuide addTfMatx(Matrix4d tf) {
		tfStack.push(tf);
		tfRecord.push("Custom transform");
		return this;
	}
	
	public Vec3d transform(Vec3d vec) {
		calculateTf();
		Vector4d vec4 = new Vector4d(vec.x, vec.y, vec.z, 1);
		tf.transform(vec4);
		return new Vec3d(vec4.x, vec4.y, vec4.z);
	}
	
	public void calculateTf() {
		tf.setIdentity();
		for (int i = tfStack.size() - 1; i >= 0; i--) {
			tf.mul(tfStack.get(i));
		}
	}

	public boolean isEmpty() {
		return (this.tfStack.size() == 0);
	}
	
	public Matrix4d getMatrix() {		
		calculateTf();
		return this.tf;
	}
	
	public ArrayList<String> getTfRecord() {
		ArrayList<String> names = new ArrayList(tfRecord);
		return names;
	}
	
	public static ArrayList<Vec3d> xformVec3dList(List<Vec3d> form, TransformGuide tf){
		ArrayList<Vec3d> product = new ArrayList();
		for (Vec3d point : form) {
			product.add(tf.transform(point));
		}
		return product;
	}
	
	/**
	 * Turns current transformation stack into one single transform
	 */
	public void flatten() {
		calculateTf();
		tfStack.clear();
		addTfMatx(new Matrix4d(tf));
	}
	
	/**
	 * Returns current transform as one single matrix
	 */
	public Matrix4d getFlattened() {
		calculateTf();
		return new Matrix4d(tf);
	}
	
	/**
	 * Adds a new transformation that is the combination of all existing ones
	 * @param pow the power to raise this xform to
	 */
	public void power(int pow) {
		if (pow > 1) {
			calculateTf();
			Matrix4d tfn = (Matrix4d) tf.clone();
			for (int i = 1; i < pow; i++)
				tfn.mul(tf);
			addTfMatx(tfn);
		}
	}
	
	public TransformGuide shallowClone() {
		TransformGuide newTf = new TransformGuide();
		newTf.addTfMatx(this.getFlattened());
		newTf.calculateTf();
		return newTf;
	}
	
	@Override
	public TransformGuide clone() {
		TransformGuide newTf = new TransformGuide();
		newTf.tfStack.clear();
		for (Matrix4d m : this.tfStack)
			newTf.tfStack.push(new Matrix4d(m));
		for (String s : this.tfRecord)
			newTf.tfRecord.push(s);
		newTf.calculateTf();
		return newTf;
	}
	
	@Override
	public String toString() {
		return this.tf.toString();
	}

	public void clear() {
		this.tfStack.clear();
	}
}
