package com.uranophane.buildguides.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class VectorManip {
	public static double getVec3dDist(Vec3d coord1, Vec3d coord2) {
		//basic Pythagorean distance
		return Math.sqrt(
				Math.pow(coord1.x - coord2.x, 2) +
				Math.pow(coord1.y - coord2.y, 2) +
				Math.pow(coord1.z - coord2.z, 2)
				);
	}
	
	public static Vec3d rotateVec3dX(Vec3d vector, double angleRad, Vec3d origin) {
		vector = vector.subtract(origin);
		Vec3d rotVec = new Vec3d(
				vector.dotProduct(new Vec3d(1, 0, 0)),
				vector.dotProduct(new Vec3d(0, Math.cos(angleRad), -Math.sin(angleRad))),
				vector.dotProduct(new Vec3d(0, Math.sin(angleRad), Math.cos(angleRad)))
				);
		return rotVec.add(origin);
	}
	
	public static Vec3d rotateVec3dY(Vec3d vector, double angleRad, Vec3d origin) {
		vector = vector.subtract(origin);
		Vec3d rotVec = new Vec3d(
				vector.dotProduct(new Vec3d(Math.cos(angleRad), 0, Math.sin(angleRad))),
				vector.dotProduct(new Vec3d(0, 1, 0)),
				vector.dotProduct(new Vec3d(-Math.sin(angleRad), 0, Math.cos(angleRad)))
				);
		return rotVec.add(origin);
	}
	
	public static Vec3d rotateVec3dZ(Vec3d vector, double angleRad, Vec3d origin) {
		vector = vector.subtract(origin);
		Vec3d rotVec = new Vec3d(
				vector.dotProduct(new Vec3d(Math.cos(angleRad), -Math.sin(angleRad), 0)),
				vector.dotProduct(new Vec3d(Math.sin(angleRad), Math.cos(angleRad), 0)),
				vector.dotProduct(new Vec3d(0, 0, 1))
				);
		return rotVec.add(origin);
	}
	
	public static Vec3d zIntersect(Vec3d origin, Vec3d vec, double z) {
		vec = vec.normalize();
		Vec3d intercept = vec.scale((origin.z - z)/vec.z);
		return intercept.add(origin);
	}
	
	public static Vec3d xIntersect(Vec3d origin, Vec3d vec, double x) {
		vec = vec.normalize();
		Vec3d intercept = vec.scale((origin.x - x)/vec.x);
		return intercept.add(origin);
	}
	
	public static Vec3d yIntersect(Vec3d origin, Vec3d vec, double y) {
		vec = vec.normalize();
		Vec3d intercept = vec.scale((origin.y - y)/vec.y);
		return intercept.add(origin);
	}
	
	public static double getAzimuth(Vec3d dirn) {
		return Math.atan2(dirn.z, dirn.x);
	}
	
	public static double getElevation(Vec3d dirn) {
		double r = Math.sqrt(dirn.x*dirn.x + dirn.z*dirn.z);
		return Math.atan2(dirn.y, r);
	}
	
	public static void appendVec3dOctuple(Set<Vec3d> pattern, Vec3d vector){
		double x = vector.x;
		double y = vector.y;
		double z = vector.z;

		pattern.add(new Vec3d(x, y, z));
		pattern.add(new Vec3d(x, -y, z));
		pattern.add(new Vec3d(-x, y, z));
		pattern.add(new Vec3d(-x, -y, z));
		pattern.add(new Vec3d(x, y, -z));
		pattern.add(new Vec3d(x, -y, -z));
		pattern.add(new Vec3d(-x, y, -z));
		pattern.add(new Vec3d(-x, -y, -z));
	}
	
	public static Vec3i offsetVec3i(Vec3i vec, int x, int y, int z) {
		return new Vec3i(vec.getX() + x, vec.getY() + y, vec.getZ() + z);
	}
	
	public static HashSet<Vec3i> getHollowVec3iSet(Set<Vec3i> pattern){
		HashSet<Vec3i> hollowSet = new HashSet();
		for (Vec3i block : pattern) {
		// add then skip the block if it touches air on any side
			if (!pattern.contains(offsetVec3i(block, 0, 0, 1)))
				hollowSet.add(block);
			else if (!pattern.contains(offsetVec3i(block, 0, 0, -1)))
				hollowSet.add(block);
			else if (!pattern.contains(offsetVec3i(block, 0, 1, 0))) 
				hollowSet.add(block);
			else if (!pattern.contains(offsetVec3i(block, 0, -1, 1))) 
				hollowSet.add(block);
			else if (!pattern.contains(offsetVec3i(block, 1, 0, 0))) 
				hollowSet.add(block);
			else if (!pattern.contains(offsetVec3i(block, -1, 0, 0))) 
				hollowSet.add(block);
		// if it doesn't touch air on any side, don't add
		}
		return hollowSet;
	}
	
	public static Collection<Vec3d> batchVec3iToVec3d(Collection<Vec3i> from, Collection<Vec3d> to){
		to.clear();
		for (Vec3i vec : from) {
			to.add(new Vec3d(vec.getX(), vec.getY(), vec.getZ()));
		}
		return to;
	}
}
